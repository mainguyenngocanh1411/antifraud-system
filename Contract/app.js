const express = require('express')
const bodyParser = require('body-parser')
var cors = require('cors');
// const permissionRouter = require('./routes/permission')
const stdRouter = require('./routes/studentRoute')
const transactionRouter = require('./routes/transactionRoute')
const encryptorRouter = require('./routes/encryptorRoute')
const app = express();
const PORT = process.env.PORT || 1411;

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

app.use(cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  
// app.use('/permissions', permissionRouter)
app.use('/std', stdRouter)
app.use('/tx', transactionRouter)
app.use('/cryptor', encryptorRouter)
app.listen(PORT, () => console.log(`Listening on ${PORT} ..`));

app.get('/', (req, res) => {
    res.send(`Listening on ${PORT}`);
})
