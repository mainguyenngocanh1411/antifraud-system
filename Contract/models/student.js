let mongoose = require('mongoose')
let studentSchema = new mongoose.Schema({
  _id: String,
  fullName: String,
  dateOfBirth: String,
  subjects: []
}, { versionKey: false })

module.exports = mongoose.model('Student', studentSchema)