let mongoose = require('mongoose')
let keySchema = new mongoose.Schema({
  _id: Number,
  privateKey: String,
  publicKey: String
}, { versionKey: false })

module.exports = mongoose.model('Key', keySchema)