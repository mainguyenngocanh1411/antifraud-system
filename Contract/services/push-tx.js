const fs = require('fs')
const Eos = require('eosjs')
const stdManager = require('./stdManager')
const StudentSchema = require('../models/student')
const chain = {
    main: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906', // main network
    jungle: 'e70aaab8997e1dfce58fbfac80cbbb8fecec7b99cf982a9444273cbc64c41473', // jungle testnet
    sys: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f' // local developer
}

const eos = Eos({
    //httpEndpoint: 'http://127.0.0.1:8888',
    httpEndpoint: 'http://jungle2.cryptolions.io:80',
    chainId: chain.jungle  
})



function generateTxData(input) {
    return {
        actions: [
            {
                account: 'nanatester12',
                name: 'submit',
                authorization: [
                    {
                        actor: 'nanatester12',
                        permission: 'active'
                    }
                ],
                data: {
                    examCode: input.exam_code,
                    subjectCode: input.subject_code,
                    studentId: input.student_id,                    
                    secretKey1: input.secretKey1,
                    secretKey2: input.secretKey2,
                    secretKey3: input.secretKey3,
                    encrypted_answers: input.data,
                    imageHash: input.imageHash
                }
            }
        ]
    }
}

async function sendTransaction(input) {

    let keySender = '5JsodgaPvpXY89eebJhoXN7B3NsRx8BHTWVHPygMgGhWiDrYwfL';
    let data = generateTxData(input)

    console.log("DATA TO WRITE ON THE BLOCKCHAIN \n",
        data.actions[0].data,
        "\n\n\n"
    )
    let student = await StudentSchema.findOne({
        _id:  data.actions[0].data.studentId
    })
  
    if(student.subjects.length==0){
        console.log()
        return eos.transaction(
            data,
            {
                keyProvider: keySender
            }
        )
            .then((txReceipt) => {
                console.log("TRANSACION RECEIPT")
                console.log(txReceipt)
                console.log("Add log to db")
                let detail = data.actions[0].data
               
                stdManager
                    .addSubject(detail.studentId, detail.subjectCode, detail.examCode, detail.encrypted_answers, txReceipt.transaction_id, txReceipt.processed.block_num,txReceipt.processed.block_time,detail.imageHash);
                return  [txReceipt.transaction_id, null];
    
            })
            .catch(err => {
                console.log(err)
                return [null, err];
            });
    }
    else{
        return [null, "Subject's data exists"]
    }
    
   
}

module.exports.sendTransaction = sendTransaction


