fs = require('fs')
Eos = require('eosjs')
const StudentSchema = require('../models/student')

chain = {
    main: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906', // main network
    jungle: 'e70aaab8997e1dfce58fbfac80cbbb8fecec7b99cf982a9444273cbc64c41473', // jungle testnet
    sys: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f' // local developer
}

const eos = Eos({
    //httpEndpoint: 'http://127.0.0.1:8888',
    httpEndpoint: 'http://jungle.eoscafeblock.com:8888',
    chainId: chain.jungle  
})

let pullTxManager ={
    getTransaction: async(input)=> {

        let stdId = input.id;
    
        return StudentSchema.findOne({
            _id: stdId
        }).then((res) => {
            if(res!=null){
                if(res.subjects[0]!=null){
                    
                    return eos.getTransaction(
                        res.subjects[0].txId,res.subjects[0].blockNumber
                     ).then((res) => {
                         console.log(res)
                         var data =  res.trx.trx.actions[0].data;
                         data["timeStamp"] = res.block_time;
                         return  [data, null];
                     }).catch((err) => {
                         return  [null, err];
                     });
                }
                else{
                    return[null, "Subject's data does not exits!"]
                }
               
            }
            else{
                return[null, "Student does not exist!"]
            }
            
        })
    
    
    },
    
    getTransactionByHash: async (input)=> {
        let txId = input.txId;
        let blockNumber = input.blockNumber;
        return eos.getTransaction(
            txId,blockNumber
         ).then((res) => {
             console.log(res)
             var data =  res.trx.trx.actions[0].data;
             data["timeStamp"] = res.block_time;
             return  [data, null];
         }).catch((err) => {
      
             return  [null, err];
         });
    
    
    }
}


module.exports = pullTxManager

