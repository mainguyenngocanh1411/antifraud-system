const NodeRSA = require('node-rsa');
const _crypto = require('crypto');
const encryptor = require('../services/encryptor')
const pullManager = require('../services/pull-tx')
const Encryptor = {
    encrypt: (message, publicKey) => {
        let key = new NodeRSA()
        key.importKey(publicKey, 'pkcs8-public');
        var result = key.encrypt(message, 'base64');
        console.log("result: ", result)
        return result;
    },
    decrypt: (encryptedAnswer, encryptedSckey, encryptedSckey2, privateKey) => {       
        let key = new NodeRSA()
        key.importKey(privateKey, 'pkcs8')    
        try{
            var scKey = key.decrypt(encryptedSckey, 'utf8');
        }
        catch(err){
            try{
                var scKey = key.decrypt(encryptedSckey2, 'utf8');
            }
            catch(err){
                console.log("error")
                return null;
            }
           
        }
       
        
        console.log(encryptedAnswer)
        return Encryptor.decryptAES(encryptedAnswer, scKey);
    },
    generateKeyPairs: () => {
        let key = new NodeRSA({ b: 512 });
        let publicKey = key.exportKey('pkcs8-public-pem');
        let privateKey = key.exportKey('pkcs8-pem');

        return { publicKey, privateKey }
    },
    /**
     * Encrypts text by given key
     * @param String text to encrypt
     * @param Buffer masterkey
     * @returns String encrypted text, base64 encoded
     */
    encryptAES: function (text, masterkey){
        // random initialization vector
        const iv = new Buffer([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

        // random salt
        const salt = _crypto.randomBytes(16);

        // derive encryption key: 32 byte key length
        // in assumption the masterkey is a cryptographic and NOT a password there is no need for
        // a large number of iterations. It may can replaced by HKDF
        // the value of 65536 is randomly chosen!
        const key = _crypto.pbkdf2Sync(masterkey, salt, 2145, 32, 'sha256');

        // AES 256 GCM Mode
        const cipher = _crypto.createCipheriv('aes-256-gcm', key, iv);

        // encrypt the given text
        const encrypted = Buffer.concat([cipher.update(text, 'utf8'), cipher.final()]);

        // extract the auth tag
        const tag = cipher.getAuthTag();

        // generate output
        return Buffer.concat([salt, tag, encrypted]).toString('base64');
    },

    /**
     * Decrypts text by given key
     * @param String base64 encoded input data
     * @param Buffer masterkey
     * @returns String decrypted (original) text
     */
    decryptAES: function (encdata, masterkey){
        // base64 decoding
        const bData = Buffer.from(encdata, 'base64');

        // convert data to buffers
        const iv = new Buffer([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        const salt = bData.slice(0, 16);
        const tag = bData.slice(16, 32);
        const text = bData.slice(32);

        // derive key using; 32 byte key length
        const key = _crypto.pbkdf2Sync(masterkey, salt , 2145, 32, 'sha256');

        // AES 256 GCM Mode
        const decipher = _crypto.createDecipheriv('aes-256-gcm', key, iv);
        decipher.setAuthTag(tag);

        // encrypt the given text
        const decrypted = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8');

        return decrypted;
    },

    getSckey: function(privateKey, stdId){
        console.log(stdId)
        return pullManager.getTransaction(stdId).then(std =>{
            if(std!=null){
             
                let key = new NodeRSA()
                key.importKey(privateKey, 'pkcs8')    
                var scKey = '';
                var seckeyKey1 = std[0].secretKey1;
                var secretKey2 = std[0].secretKey2;
                try{
                    scKey= key.decrypt(seckeyKey1, 'utf8');
                }
                catch(err){
                    try{
                        console.log("Key 2: " + secretKey2)
                        scKey = key.decrypt(secretKey2, 'utf8');
                        console.log("sckey " + scKey)
                    }
                    catch(err){
                        console.log("error: " + err)
                        return null;
                    }
                   
                }
                return scKey;
            }
            else{
                return null;
            }
        })
    }
}

module.exports = Encryptor