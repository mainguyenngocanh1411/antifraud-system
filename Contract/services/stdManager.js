const StudentSchema = require('../models/student')
const KeySchema = require('../models/key')
const db = require('../constants/database')
const ExaminerKeySchema = require('../models/examinerKey')
const encryptor = require('../services/encryptor')
let studentManager = {

    createStudentRecord: async (_id, _name, _dateOfBirth) => {
        console.log(_id)
        return StudentSchema.findOne({
            _id: _id
        }).then(async p => {
            console.log(p)
            if (p != null) {
                return [null, "Student ID " + _id + " is already exist"]
            } else {
            
                let user = new StudentSchema({
                    _id: _id,
                    fullName: _name,
                    dateOfBirth: _dateOfBirth
                })

                //Generate key for both students and Examiners
                let publicKey, privateKey, ePublicKey, EPrivateKey;
                var keyPair = encryptor.generateKeyPairs();
                var EKeyPair = encryptor.generateKeyPairs();
                publicKey = keyPair.publicKey;
                privateKey = keyPair.privateKey;
                EPublicKey = EKeyPair.publicKey;
                EPrivateKey = EKeyPair.privateKey;

                let key = new KeySchema({
                    _id: _id,
                    publicKey: publicKey,
                    privateKey: privateKey
                })
                let Ekey = new ExaminerKeySchema({
                    _id: _id,
                    publicKey: EPublicKey,
                    privateKey: EPrivateKey
                })
                var EnewKey = Ekey.save()
                var newKey = key.save();
                console.log("Student key: " + newKey);
                console.log("Examiner key: " + EnewKey);
                var student = await user.save();
                return [student, null];
            }
        })


    },
    getStudents: async (pageNumber, sort) => {
        if (pageNumber == 0) pageNumber = 1;
        var pageSize = 10;
        var students;
      
        if (sort != undefined) {
            var sortKey = sort.split(":")[0];
            var sortValue = parseInt(sort.split(":")[1]);
            if (sortKey == "_id") {
                students = await StudentSchema.find().sort({ _id: sortValue }).skip(pageNumber * pageSize - pageSize).limit(pageSize);
            }
            else if (sortKey == "fullName") {
                students = await StudentSchema.find().sort({ fullName: sortValue }).skip(pageNumber * pageSize - pageSize).limit(pageSize);
            }
            
        }

        else {
            students = await StudentSchema.find().skip(pageNumber * pageSize - pageSize).limit(pageSize);
        }
        var totalStudents = await StudentSchema.count();

        return { total: Math.ceil(totalStudents / pageSize), students: students };
    },

    getStudent: async (stdId) => {
        let key = await (KeySchema.findOne({ _id: stdId }));

        if (key != null) {
            let publicKey = key.publicKey;
            var student = await StudentSchema.findOne(
                {
                    _id: stdId
                })
            var returnData = {
                stdId: student._id,
                name: student.fullName,
                dateOfBirth: student.dateOfBirth,
                publicKey: publicKey
            }
            return returnData;
        }
        else {
            return null;
        }

    },

    addSubject: (stdId, subjectCode, examCode, answer, txId, blockNumber, timeStamp, imageHash) => {

        return StudentSchema.findOne(
            {
                _id: stdId
            }).then(std => {
                if (std != null) {
                    std.subjects.push({
                        subjectCode: subjectCode,
                        examCode: examCode,
                        answer: answer,
                        txId: txId,
                        blockNumber: blockNumber,
                        timeStamp: timeStamp,
                        imageHash: imageHash
                    })
                    std.save()
                    return true
                } else {
                    return false
                }
            })
    },

    getExaminerKey: async (stdId) =>{
        console.log("id is")
        let key = await (ExaminerKeySchema.findOne({ _id: stdId }));
        return key;
    }

}

module.exports = studentManager