const express = require('express')
const router = express.Router()
const manager = require('../services/stdManager')

router.post('/', async (req, res) => {
   
   
    let stdId = req.body.stdId
    let name = req.body.name
    let dateOfBirth = req.body.dateOfBirth
    var result, err;
    [result, err] = await manager.createStudentRecord(stdId, name, dateOfBirth)
    
    if(result!=null){
        res.status(200).json({success: "added user " + stdId})
    }
    else{
        res.status(400).json({error: err})
    }
  
})

router.get('/:id', (req, res) => {
    
    let _id = req.params.id;
    manager.getStudent(_id).then(std =>{
        
        if(std!=null) return res.send(std)
        else return res.send("Student is not exist!")
       
    }
      
    )
})
router.get('/key/:id', (req, res) => {
    console.log("hello")
    manager.getExaminerKey(req.params.id).then(key=>{
        console.log(key)
        return res.send(key)
    })
})
router.get('/page/:page', (req, res) => {
    var page = req.params.page || 1
    var sort = req.query.sort
    manager.getStudents(page,sort).then(std =>
        {
            var listStudents = [];
            std.students.forEach(student => {
                listStudents.push(student)
            })
            res.status(200).json(
                {total: std.total, listStudents: listStudents}
            )

        }
     
    )
})



router.post('/add', (req, res) => {
    let stdId = req.body.stdId
    let subjectCode = req.body.subjectCode
    let examCode = req.body.examCode
    let answer = req.body.answer
    let keys = req.body.keys

    // manager.addSubject(stdId, subjectCode, examCode, answer, keys)
    // .then(result => {
    //   if (result) {
    //     let mess = { success: "added subject " + subjectCode + " to permission " + stdId }
    //     res.status(200).json(mess)
    //   } else {
    //     res.status(400).json({error: "stdId is not found or subject code is duplicated!"})
    //   }
    // })
  })

module.exports = router
