const express = require('express')
const router = express.Router()
const encryptor = require('../services/encryptor')

router.post('/encrypt', (req, res) => {
    let message = req.body.message
    let publicKey = req.body.publicKey

    let keys = encryptor.encrypt(message, publicKey)

    res.status(200).json(keys)
})

router.post('/decrypt', (req, res) => { 

    
    let encryptedAnswer = req.body.encryptedAnswer;
    
    let encryptedSckey = req.body.encryptedSckey;
    let encryptedSckey2 = req.body.encryptedSckey2;
    let privateKey = req.body.privateKey.replace(/\\n/g, '\n')

    let decryptedMessage = encryptor.decrypt(encryptedAnswer, encryptedSckey, encryptedSckey2, privateKey)
    if(decryptedMessage==null){
        decryptedMessage="wrong key!"
    }
    res.status(200).json(decryptedMessage)
})

router.post('/getSckey', (req, res) => { 
    
    let privateKey = req.body.privateKey.replace(/\\n/g, '\n')
    let stdId = req.body.stdId
    console.log("hi" + stdId)
    encryptor.getSckey(privateKey, {id: stdId}).then(scKey=>{
 
        if(scKey==null){
            scKey="wrong key!"
        }
        res.status(200).json(scKey)
    });
   
})
router.post('/encryptAES', (req, res) => {
    let message = req.body.message
    let scKey = Buffer.from(req.body.scKey, 'utf8');

    let keys = encryptor.encryptAES(message, scKey)

    res.status(200).json(keys)
})

router.post('/decryptAES', (req, res) => {
    let encryptedMessage = req.body.encryptedMessage
    let scKey = Buffer.from(req.body.scKey, 'utf8');

    let decryptedMessage = encryptor.decryptAES(encryptedMessage, scKey)

    res.status(200).json(decryptedMessage)
})

router.get('/', (req, res) => {

    let keys = encryptor.generateKeyPairs()

    res.status(200).json(keys)
})

module.exports = router
