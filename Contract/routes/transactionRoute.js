const express = require('express')
const router = express.Router()
const pushManager = require('../services/push-tx')
const pullManager = require('../services/pull-tx')

router.post('/push', async (req, res) => {
    var result, err;
    [result, err] = await pushManager.sendTransaction(req.body);
    if (result!=null) {

        let mess = { success: "pushed transaction " + result}
        res.status(200).json(mess)
        
    } else {               
        res.status(400).json({ error: "Pushing transaction to EOS: " + err })
    }
})
router.post('/pull', async (req, res) => {
    var result, err;
    [result, err] = await pullManager.getTransaction(req.body)
    if (result!=null) {   
        console.log(result)             
        res.status(200).json(result)
        
    } else {               
        res.status(400).json({ error: err })
    }
})
router.post('/pullHash', async (req, res) => {
    var result, err;
    [result, err] = await pullManager.getTransactionByHash(req.body)
    if (result!=null) {                
       
        res.status(200).json(result)
        
    } else {               
        console.log("error", err)
        res.status(400).json({ error: "Transaction not found!" })
    }
})
module.exports = router
