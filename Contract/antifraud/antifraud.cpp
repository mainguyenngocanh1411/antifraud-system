#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/vector.hpp>

using namespace eosio;
using std::string;


class antifraud : public eosio::contract
{
public:
  using contract::contract;
  
  [[eosio::action]]
  void submit(std::string examCode,
                std::string subjectCode,
                std::string studentId,                
                std::string secretKey1, //Encrypted secret key
                std::string secretKey2, //Encrypted secret key
                std::string secretKey3, //Encrypted secret key                    
                std::string encrypted_answers,
                std::string imageHash)
  {
    print("--------------SUBMIT INFORMATION--------------");
    print("   Examination code: ", examCode);
    print("Subject code:      ", subjectCode);
    print("   ,   Student id:     ", studentId);  
    print("   ,   Additional key 1:       ", secretKey1);
    print("   ,   Additional key 2:   ", secretKey2);
    print("   ,   Additional key 3:   ", secretKey3);
    print("   ,   Answers:    ", encrypted_answers);
     print("   ,   Image hash:    ", imageHash);
    print("   ]");

  }
};

EOSIO_ABI(antifraud, (submit))

