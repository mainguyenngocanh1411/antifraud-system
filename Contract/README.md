Start node locally:

nodeos -e -p eosio --plugin eosio::chain_api_plugin --plugin \
eosio::history_api_plugin –contracts-console --hard-replay

-----------------------------------------------

Open default wallet:

cleos wallet open -n name_of_wallet

------------------------------------------------

Show wallet list:

cleos wallet list

------------------------------------------------

Unlock wallet:

cleos wallet unlock
Master key of default: PW5J76oxHJNRDuDjANHJdh6ELufMSHkncfBjwaGJc4vMzFGksDems
Master key of nana: PW5JKf2UGsHZWZEnNQo45YYWVMdJpk4M7L55iT3877f4NBMrrxnuL (current)

------------------------------------------------
Create key:
cleos create key


Private key: 5KGbzuKEg5wWYnLz9YX4XujeUHwPqqrfbL31RDGYbDBt68g7xbo
Public key: EOS52tnWCdtXX89914pgG93sPgiHb5zeM8KL78mJkkdKCnQMdcQVs


------------------------------------------------

Import keys to wallet:
```
cleos wallet import -n nana --private-key 5KGbzuKEg5wWYnLz9YX4XujeUHwPqqrfbL31RDGYbDBt68g7xbo
```

------------------------------------------------

Import development key (eosio):

cleos wallet import
eosio key: 5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3


-------------------------------------------------
Create account from a public key:

cleos create account eosio af.system EOS52tnWCdtXX89914pgG93sPgiHb5zeM8KL78mJkkdKCnQMdcQVs -p eosio@active

Get account information:

cleos get account account_name

current account name: af.system

------------------------------------------------

Compile file .cpp:
eosio-cpp -abigen -o antifraud.wasm antifraud.cpp


-------------------------------------------------
Upload contract:

cleos set contract af.system antifraud -p af.system@active
hash: 602068c37cc0ce3b6e957996cd5bb7b3e1d145d303c4974cd7d9b977e2d79fad
block: 546
-------------------------------------------------
Test contract:
cleos push action af.system submit '["001","VL1","1552019","abc","dce","asd","abcdef","hash"]' -p af.system@active	

--------------
Jungle testnet
Public key: EOS5Q4nCcsxoDsBwXXm4EJ1tDZViXh9Ma5opYAZmGnjefDAYi1xRZ
Private key: 5JsodgaPvpXY89eebJhoXN7B3NsRx8BHTWVHPygMgGhWiDrYwfL

Account: nanatester12

Contract: antifraud
Hash: 770d5eecd5ffdc26e2ec5fe26a575701c4f5798497969051fdb2705f8917f31e

Test:
cleos -u http://jungle2.cryptolions.io:80 push action nanatester12 submit '["001","VL1","1552019","abc","dce","asd","abcdef","hash"]' -p nanatester12@active	
