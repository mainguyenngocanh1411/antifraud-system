import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Student } from "../models/student";

import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-student-pop-up',
  templateUrl: './student-pop-up.component.html',
  styleUrls: ['./student-pop-up.component.css']
})
@Injectable()
export class StudentPopUpComponent implements OnInit {

  form: FormGroup;

  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<StudentPopUpComponent>,
    @Inject(MAT_DIALOG_DATA) data: Student) {

    this.form = this.fb.group({
      studentId: [data.studentId, Validators.required],
      fullName: [data.fullName, Validators.required],
      dateOfBirth: [data.dateOfBirth, Validators.required],   
      // score1: [data.score1, Validators.required],
      // score2: [data.score2, Validators.required],
      // score3: [data.score3, Validators.required],
      // hash1: [data.hash1, Validators.required],
      // hash2: [data.hash2, Validators.required],
      // hash3: [data.hash3, Validators.required],
    
    });
  }
  ngOnInit() {

  }

  save() {
   
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }
}
