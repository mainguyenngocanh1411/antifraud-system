import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecryptPopupComponent } from './decrypt-popup.component';

describe('DecryptPopupComponent', () => {
  let component: DecryptPopupComponent;
  let fixture: ComponentFixture<DecryptPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecryptPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecryptPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
