import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LookupComponent } from '../lookup/lookup.component';

@Component({
  selector: 'app-decrypt-popup',
  templateUrl: './decrypt-popup.component.html',
  styleUrls: ['./decrypt-popup.component.css']
})
export class DecryptPopupComponent implements OnInit {
  form: FormGroup;
  result: String;
  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DecryptPopupComponent>,
    @Inject(MAT_DIALOG_DATA) data: Object) {
    this.form = this.fb.group({
      privateKey: ['', Validators.required],
      encryptedSckey: [data["encryptedSckey"], Validators.required],
      encryptedSckey2: [data["encryptedSckey2"], Validators.required],
      encryptedAnswer: [data["encryptedAnswer"], Validators.required]
    });

  }

  ngOnInit() {
  }

  save() {
    console.log(this.form)
    var req = {
      "encryptedSckey": this.form.value["encryptedSckey"],
      "encryptedSckey2": this.form.value["encryptedSckey2"],
      "encryptedAnswer": this.form.value["encryptedAnswer"],
      "privateKey": this.form.value["privateKey"]
    }
    console.log(req)
    this.http.post("http://localhost:1411/cryptor/decrypt", req)
      .subscribe(
        (val) => {
          console.log("POST call successful value returned in body",
            val);
            this.result = val.toString();
        },
        response => {
          console.log("POST call in error", response);
        },
        () => {
          console.log("The POST observable is now completed.");
        });
    

   
  }

  close() {
    this.dialogRef.close();
  }

}
