import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentsComponent }      from './students/students.component';
import {LookupComponent} from './lookup/lookup.component';
const routes: Routes = [ 
  { path: 'students', component: StudentsComponent},
  { path: 'students/:page', component: StudentsComponent},
  { path: 'lookup', component: LookupComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
