import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { StudentPopUpComponent } from '../student-pop-up/student-pop-up.component';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.css']
})

export class StudentTableComponent implements OnInit {
  public students: [];
  public txId = [];
  public totalPages = 0;
  constructor(private dialog: MatDialog, private http: HttpClient, private route: ActivatedRoute) {

  }

  ngOnInit() {
    // Make the HTTP request:
    this.route.paramMap.subscribe(params => {
      console.log("query", params)
      var pageNumber = params.get("page");
      if (pageNumber == null) pageNumber = "1"
      this.http.get('http://localhost:1411/std/page/' + pageNumber).subscribe(data => {

        this.students = data["listStudents"];
        console.log(this.students)
        this.totalPages = data["total"];


      });
    });


  }


  openDialog(): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "800px";
    dialogConfig.height = "fit-content";
    dialogConfig.data = {

    }


    const dialogRef = this.dialog.open(StudentPopUpComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => {
        console.log("Dialog output:", data)
        var student = {
          "stdId": data["studentId"],
          "name": data["fullName"],
          "dateOfBirth": data["dateOfBirth"]
        }
        this.http.post("http://localhost:1411/std", student)
        .subscribe(
          (val) => {
            console.log("POST call successful value returned in body",
              val);
          },
          response => {
            console.log("POST call in error", response);
            alert("Error: " + response.error.error);
          },       
          () => {
            console.log("The POST observable is now completed.");
            
          });
      }

    );
  }

  array(): any[] {
    return Array(this.totalPages);
  }
  CopyToClipboard(index) {
    console.log(this.students[index])
    const value = this.students[index].subjects[0].txId;
    
    const el = document.createElement('textarea');
    el.value = value;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    alert("Transaction id has been copied to clipboard")
    
    
  }
}
