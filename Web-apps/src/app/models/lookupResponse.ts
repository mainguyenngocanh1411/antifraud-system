export class LookupResponse {
    studentId:number;
    examCode: String;
    subjectCode:String;
    encryptedAnswer: String;
    timeStamp: String;
    secretKey1: String;
    secretKey2: String;
  
}
