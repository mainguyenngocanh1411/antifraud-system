import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { LookupResponse } from '../models/lookupResponse';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { DecryptPopupComponent } from '../decrypt-popup/decrypt-popup.component';
@Component({
  selector: 'app-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.css']
})
export class LookupComponent implements OnInit {
  form: FormGroup;
  lookupResponse: LookupResponse;
  nrSelect = 0;
  constructor(private dialog: MatDialog, private http: HttpClient) {
    this.lookupResponse = new LookupResponse();
  }

  ngOnInit() {
  }
  onClickSubmit(data) {
    var req, url;
    console.log("Data", data)
    if (data.mode == 0) {
      req = {
        id: data["id"]
      }
      url = "http://localhost:1411/tx/pull"

    }
    else {
      req = {
        txId: data["txHash"],
        blockNumber: data["blockNumber"]
      }
      url = "http://localhost:1411/tx/pullHash"
    }

    this.http.post(url, req)
      .subscribe(
        (val) => {
          console.log("POST call successful value returned in body",
            val);
          this.lookupResponse.studentId = val["studentId"]
          this.lookupResponse.examCode = val["examCode"]
          this.lookupResponse.subjectCode = val["subjectCode"]
          this.lookupResponse.encryptedAnswer = val["encrypted_answers"]
          this.lookupResponse.timeStamp = val["timeStamp"]
          this.lookupResponse.secretKey1 = val["secretKey1"].replace(/\"/g, "");
          this.lookupResponse.secretKey2 = val["secretKey2"].replace(/\"/g, "");
        },
        response => {
          console.log("POST call in error", response.error);
          alert("Error: " + response.error.error);

        },
        () => {
          console.log("The POST observable is now completed.");
        });

  }
  onChange(value) {

    var test = document.getElementById("search-by-hash");
    var stdId = document.getElementById("std-id");
    if (value == 1) {

      test.style.display = "";
      stdId.style.display = "none";
    }
    else {
      stdId.style.display="";
      test.style.display = "none";
    }
  }
  openDialog(): void {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "800px";
    dialogConfig.height = "fit-content";
    dialogConfig.data = {
      encryptedSckey : this.lookupResponse.secretKey1,
      encryptedSckey2: this.lookupResponse.secretKey2,
      encryptedAnswer: this.lookupResponse.encryptedAnswer

    }
    const dialogRef = this.dialog.open(DecryptPopupComponent, dialogConfig);
  }

}
