Release port: 7474

Dev port: 6123

## APIs
Currently open api:
 
### Get data from transaction ID
http://115.165.166.126:6123/api/getDataFromTxId

Deprecated temporarily

### Get transaction list from SHIPMENT_ID
http://115.165.166.126:6123/api/getTransactionListFromShipmentId

Input:
```
{
    "SHIPMENT_ID": "12312124124124"
}
```

Output:
```
{
    "TRANSACTION_ID": [
        "SAMPLE_TX_ID1",
        "SAMPLE_TX_ID2"
    ]
}
```

### Push Transaction
http://115.165.166.126:6123/api/pushTx
- Support for STM / STB
- Data format

Input:
```
{
    "USER_ID": ["ID1", "ID2"],
    "GROUP_ID": [],
    "DATA": {
        // Whatever data is 
    }
}
```

**TO DO**
- [ ] `TRANSACTION_ID` field for updating `SHIPMENT` must contains only new `TRANSACTION_ID`s 

### Add public key
http://115.165.166.126:6123/api/addPublicKey


### Generating key pair
http://115.165.166.126:6123/api/generateKeyPair

This API will be deprecated in next version. Check out #dev-service-encrypt for encryption library     