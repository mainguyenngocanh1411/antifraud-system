fs = require('fs')
Eos = require('eosjs')

const LOG_DIR = '/opt/JungleTestnet/smartlog-backend/dev-service-blockchain/logReceipt/'
const DATA_DIR = '/opt/JungleTestnet/smartlog-backend/dev-service-blockchain/data/'

chain = {
    main: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906', // main network
    jungle: '038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca', // jungle testnet
    sys: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f' // local developer
}

eos = Eos({
    httpEndpoint: 'http://127.0.0.1:8888',
    // httpEndpoint: 'http://jungle.cryptolions.io:18888',
    chainId: chain.jungle,
    keyProvider: '5Jy3e1igN7yLqmYUY5Zdr8dAe18gEZzAeEJTbsA4g7ibKxRuo91'
})

// logReceipt = JSON.parse(fs.readFileSync(LOG_DIR + '12.json', {encoding: 'utf-8'}))

async function getTransaction(txId) {

    return eos.getTransaction(
        txId
    ).then((ret) => {
        // console.log(JSON.stringify(ret,null,2))
        console.log(ret.trx.trx.actions[0].data);
        return ret.trx.trx.actions[0].data;
    }).catch( (err) => {
        console.log(err)
    });
}

async function getRawTransaction(txId) {
    return eos.getTransaction(
        txId
    ).then((ret) => {
       return ret
    }).catch( (err) => {
        console.log(err)
    });
} 

// pullTransaction()

module.exports.getTransaction    = getTransaction
module.exports.getRawTransaction = getRawTransaction
