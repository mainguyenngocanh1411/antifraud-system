
#include <eosiolib/eosio.hpp>
#include <eosiolib/print.hpp>
#include <eosiolib/vector.hpp>

using namespace eosio;
using std::vector;
using std::string;

class smartlog : public eosio::contract {
  public:
      using contract::contract;

      struct access {
        string id;
        string key;
      };

      /// @abi action
      void senddata( std::string shipment_id, std::string key_for_owner, std::string key_for_active, std::string key_for_group, std::string key_for_whse, std::string key_for_customer, std::string key_for_supplier, std::string key_for_carrier, std::string encrypted_data ) {
        print ( "   SHIPMENT_ID: ", shipment_id );

        print ( "   ENCRYPTED_SCPASSWORD [  " );

        print (        "OWNER:      ", key_for_owner     );
        print ( "   ,   ACTIVE:     ", key_for_active    );
        print ( "   ,   GROUP:      ", key_for_group     );

        print ( "   ,   WHSE:       ", key_for_whse      );
        print ( "   ,   CUSTOMER:   ", key_for_customer  );
        print ( "   ,   SUPPLIER:   ", key_for_supplier  );
        print ( "   ,   CARRIER:    ", key_for_carrier   );
        print ( "   ]" );
        print ( "   ENCRYPTED_DATA", encrypted_data );
      }

      void senddatadev( string shipment_id, 
                     access owner,
                     access active,
                     vector<access>& users,
                     vector<access>& groups,
                     string encrypted_data )
      {
        /// EMPTY HERE
        /// Printing info is now disabled since it is not included inside block data
        print ( "senddata_dev is triggered" );
      }
};

EOSIO_ABI( smartlog, (senddata)(senddatadev) )
//EOSIO_ABI( smartlog, (senddata_dev) )

