const express = require('express');
const app = express();
const router = express.Router();
const cors = require('cors')
const bodyParser = require('body-parser');
const fs = require('fs')

const pushTx = require('./push-tx')
const pullTx = require('./pull-tx')
const SCPassHelper = require('../dev-service-encrypt/src/scpassword-helper')
const EncryptService = require('../dev-service-encrypt/src/push-data-to-blockchain')
const DecryptService = require('../dev-service-encrypt/src/pull-data-from-blockchain')
const GenKeyService = require('../dev-service-encrypt/src/private-and-public-keys-generator')
const DatabaseBridge = require('./connectDB')

const LOG_DIR = __dirname + '/logReceipt/'
const DATA_DIR = __dirname + '/data/'

const port = 6123;


app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(cors())

// url: http://localhost:6123/
app.get('/', (request, response) => response.send('SmartLog'));

app.get ('/', (request, response) => {
  response.json({message: 'Hello, welcome to SmartLog'});
});

// set the server to listen on port 6123
app.listen(port, () => console.log(`Listening on port ${port}`));






app.post('/api/pushTx', async (request, response) => {
    var input = request.body;

    console.log("[] Incoming pushTx request... ")
    console.log(input)
    console.log("\n\n\n")

    /* Process input */

    DatabaseBridge.postNewShipmentLog(input, function (error, shipment_id) {

        console.debug(shipment_id)

        EncryptService.generateData(input, shipment_id)
            .then(dataToWrite => {
                if (dataToWrite.ERROR !== undefined) {
                   response.json(dataToWrite)
                }
                console.log("dataToWrite: ", dataToWrite)
	        return pushTx.sendTransaction(dataToWrite)
            })

        /* Send tracsaction to blockchain */
            .then(ret => {
                response.status(200).json(ret)
                console.log(ret)
            })
            .catch(err => {
                console.error(err)
                response.status(500).send(err.toString())
            })
    })
});

app.post('/api/getTransactionListFromShipmentId', (request, response) => {
    console.log("[] Incoming getTransactionListFromShipmentId request...");
    console.log(request.body);

    SHIPMENT_ID = request.body.SHIPMENT_ID;

    output = {};

    DatabaseBridge.getTxListFromShipmentID(SHIPMENT_ID, function(error, txList) {
        if (error) {
            response.status(400).send(error)
            return;
        }
    console.log(txList)
    output.SHIPMENT_ID = SHIPMENT_ID;
    output.TRANSACTION_ID = txList.TRANSACTION_ID

        response.status(200).json(output)
    });
})

app.post('/api/getRawTxFromTxId', (request, response) => {
    console.log("[] Incoming getRawTxFromTxId request...");
    console.log(request.body);

    TRANSACTION_ID = request.body.TRANSACTION_ID;

    pullTx.getTransaction(TRANSACTION_ID)
    .then((rawTx) => {
        response.status(200).json(rawTx);
    })
});

app.post('/api/getShipmentInfoFromTxId', (req, res) => {
    console.log("[] Incoming getShipmentInfoFromTxId request...");
    console.log(req.body);

    TRANSACTION_ID = req.body.TRANSACTION_ID;

    pullTx.getRawTransaction(TRANSACTION_ID)
    .then((rawTx) => {
        console.log(rawTx);
	TRANSACTION_DATA = rawTx.trx.trx.actions[0].data;
        ret_json = {
            TRANSACTION_ID: TRANSACTION_ID,
            TIMESTAMP: rawTx.block_time,
            ENCRYPTED_DATA: TRANSACTION_DATA.encrypted_data
        }
        console.log("encrypted_data", TRANSACTION_DATA.encrypted_data)
        console.log("active_key", TRANSACTION_DATA.active.key)

        ret_json.DECRYPTED_DATA = JSON.parse(DecryptService.decryptByUserId(
            {
                data: TRANSACTION_DATA.encrypted_data,
                key: TRANSACTION_DATA.active.key
            },
            'ACTIVE'
        ))

        return ret_json;
    })
    .then((ret_json) => {
        console.log(ret_json);
        res.status(200).json(ret_json);
    })
    .catch((error) => {
        console.log(error);
        res.status(500).send(error);
    })
});

/**
 * @param {string}: USER_ID to add key
 * @param {string}: PUBLIC_KEY (generate from ECIES algorithm) with 160-character of length  
 * @returns {JSON}: Nofification conresponding to success/failure
 */
app.post('/api/addPublicKey', (request, response) => {
    var input = request.body;

    USER_ID = input.USER_ID
    PUBKEY  = input.PUBLIC_KEY

    console.log(input)

    // Check if key is valid
    if (PUBKEY.slice(0,2) == '0x')
        PUBKEY = PUBKEY.slice(2)
    if (PUBKEY.length !== 130) {
        console.log ("[] ERROR: PUBKEY must be 160 characters length")
        response.json({
            "ERROR": "PUBKEY must be 160 characters length"
        })
	return
    }

    try {
	    SCPassHelper.encryptSCPassword("deadbeef","3f9ee2e5556727e2f0f1f2d2a9989b0a0612f6367782045e9bc2e09dcd1f0c60", PUBKEY)
    }
    catch (error) {
        console.log ("[] ERROR: Weak public key; Invalid DER format public key")
        console.error(error)
	    response.json({
            "ERROR": "USER_ID Weak public key; Invalid DER format public key"
        })
	    return
    }


    // Check if USER_ID is existed
    KEY_DIR = __dirname + '/../dev-service-encrypt/key'
    keyfile_list = fs.readdirSync(KEY_DIR)

    currentUserKeys = keyfile_list.filter((filename) => {
        if (filename.indexOf(USER_ID) !== -1)
            return true
        })

    if (currentUserKeys.length !== 0) {
        console.log ("[] ERROR: USER_ID existed")
        response.json({
            "ERROR": "USER_ID existed"
        })
	return
    }

    // Add to file
    fs.writeFileSync(KEY_DIR + '/' + USER_ID + '-pbkey.txt', PUBKEY)
    console.log("[] SUCCESS: Write new PUBKEY for " + USER_ID + " to " + KEY_DIR + '/' + USER_ID + '-pbkey.txt',)
    response.json({
        "SUCCESS": "New PUBKEY is saved for user " + USER_ID
    })
});

/**
 * @param {string}: USER_ID to add key
 * @param {string}: PUBLIC_KEY (generate from ECIES algorithm) with 130-character of length  
 * @param {string}: PRIVATE_KEY (generate from ECIES algorithm) with 130-character of length
 * @returns {JSON}: Nofification conresponding to success/failure
 */
app.post('/api/addKeyPair', (request, response) => {
    var input = request.body;

    USER_ID = input.USER_ID
    PUBKEY  = input.PUBLIC_KEY
    PRVKEY  = input.PRIVATE_KEY

    console.log(input)

    // Check if key is valid
    if (PUBKEY.slice(0,2) == '0x')
        PUBKEY = PUBKEY.slice(2)
    if (PUBKEY.length !== 130) {
        console.log ("[] ERROR: PRIVATE_KEY must be 130 characters length")
        response.json({
            "ERROR": "PRIVATE_KEY must be 130 characters length"
        })
	    return
    }

    if (PRVKEY.slice(0,2) == '0x')
        PRVKEY = PRVKEY.slice(2)
    if (PRVKEY.length !== 64) {
        console.log ("[] ERROR: PRIVATE_KEY must be 64 characters length")
        response.json({
            "ERROR": "PRIVATE_KEY must be 64 characters length"
        })
        return
    }

    try {
	    SCPassHelper.encryptSCPassword("deadbeef","3f9ee2e5556727e2f0f1f2d2a9989b0a0612f6367782045e9bc2e09dcd1f0c60", PUBKEY)
    }
    catch (error) {
        console.log ("[] ERROR: Weak public key; Invalid DER format public key")
        console.error(error)
	    response.json({
            "ERROR": "USER_ID Weak public key; Invalid DER format public key"
        })
	    return
    }

    // TODO check DER format of privatekey

    // Check if USER_ID is existed
    KEY_DIR = __dirname + '/../dev-service-encrypt/key'
    keyfile_list = fs.readdirSync(KEY_DIR)

    currentUserKeys = keyfile_list.filter((filename) => {
        if (filename.indexOf(USER_ID) !== -1)
            return true
        })

    if (currentUserKeys.length !== 0) {
        console.log ("[] ERROR: USER_ID existed")
        response.json({
            "ERROR": "USER_ID existed"
        })
	return
    }

    // Add to file
    fs.writeFileSync(KEY_DIR + '/' + USER_ID + '-pbkey.txt', PUBKEY)
    console.log("[] SUCCESS: Write new PUBLIC for " + USER_ID + " to " + KEY_DIR + '/' + USER_ID + '-pbkey.txt',)
    fs.writeFileSync(KEY_DIR + '/' + USER_ID + '-pvkey.txt', PRVKEY)
    console.log("[] SUCCESS: Write new PRIVATE_KEY for " + USER_ID + " to " + KEY_DIR + '/' + USER_ID + '-pvkey.txt',)
    response.json({
        "SUCCESS": "New KEY PAIR is saved for user " + USER_ID
    })
})

/**
 * This abi generates keypair only. Server does not keep generated keys. 
 * Clients MUST import their PUBKEY(s) for later use.
 * 
 * @returns {JSON}: Key pair in plaintext
 */
app.post('/api/generateKeyPair', (request, response) => {
    ret = GenKeyService.generatePairKey()
    response.json({
        "PRIVATE_KEY": ret.privateKey.toString('hex'),
	    "PUBLIC_KEY":  ret.publicKey.toString('hex')
    })
})

