fs = require('fs')
Eos = require('eosjs')

const DatabaseBridge = require('./connectDB')

const LOG_DIR = '/opt/JungleTestnet/smartlog-backend/dev-service-blockchain/logReceipt/'
const DATA_DIR = '/opt/JungleTestnet/smartlog-backend/dev-service-blockchain/data/'

const chain = {
    main: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906', // main network
    jungle: '038f4b0fc8ff18a4f0842a8f0564611f6e96e8535901dd45e43ac8691a1c4dca', // jungle testnet
    sys: 'cf057bbfb72640471fd910bcb67639c22df9f92470936cddc1ade0e2f2e7dc4f' // local developer
}

const eos = Eos({
 //   httpEndpoint: 'http://127.0.0.1:8888',
 //   httpEndpoint: 'http://54.233.222.22:8886',
    httpEndpoint: 'http://jungle.cryptolions.io:18888',
    chainId: chain.jungle,
    keyProvider: '5Jy3e1igN7yLqmYUY5Zdr8dAe18gEZzAeEJTbsA4g7ibKxRuo91'
})

// input = JSON.parse(fs.readFileSync(DATA_DIR + 'sampleInput2.json', {encoding: 'utf-8'}))


function generateLog(input, txReceipt) {
    return {
        SHIPMENT_ID: input.SHIPMENT_ID.toString(),
        USER_ID: input.USERS,
        GROUP_ID: input.GROUPS,
        TRANSACTION_ID: [ txReceipt.transaction_id ]
    }
}

function generateTxData(input) {
    return {
        actions: [
            {
                account: 'sctestaaaaaa',
                name: 'senddatadev',
                authorization: [
                    {
                    actor: 'sctestaaaaaa',
                    permission: 'active'
                    }
                ],
                data: {
                    shipment_id:   input.SHIPMENT_ID,
                    owner:  {
                        id: input.OWNER.ID,
                        key: input.OWNER.KEY
                    },
                    active: {
                        id: input.ACTIVE.ID,
                        key: input.ACTIVE.KEY
                    },
                    users:  input.USERS.map((access) => {
                                return {
                                    id: access.ID,
                                    key: access.KEY
                                }
                            }),
                    groups: input.GROUPS.map((access) => {
                        return {
                            id: access.ID,
                            key: access.KEY
                        }
                    }),
                    encrypted_data: input.DATA
                }
            }
        ]
    }
}

async function sendTransaction(input) {

    let keySender =  '5Jvtxnsg5phR3JC7UVk8FDR2PsGQpZmD1eXn5QeJfP7VzqPDGV3';
//   console.log(input)
    let data =  generateTxData(input)

    console.log("DATA TO WRITE ON THE BLOCKCHAIN \n",
                 data.actions[0].data,
                "\n\n\n"
    )

    return eos.transaction(
        data,
        {
            keyProvider: keySender
        }
    )
    .then((txReceipt) => {
        console.log("TRANSACION RECEIPT")
        console.log(txReceipt)

        let log = generateLog(input, txReceipt)
        DatabaseBridge.alterShipmentLog(log)

        return log;
    })
    .catch( function(err) {
        console.log(err)
    });
}

module.exports.sendTransaction = sendTransaction


function main() {
    let keySender =  '5Jvtxnsg5phR3JC7UVk8FDR2PsGQpZmD1eXn5QeJfP7VzqPDGV3';
    eos.transaction(
        {actions: [
            {
                account: 'sctestaaaaaa',
                name: 'senddatadev',
                authorization: [{
                    actor: 'sctestaaaaaa',
                    permission: 'active'
                }],
                data: {
                    shipment_id: "1",
                    owner: {id: "owner", key: "hi"},
                    active: {id: "active", key: "pi"},
                    users: [
                        {
                            id:  "aa",
                            key: "AA"
                        },
                        {
                            id:  "bb",
                            key: "BB"
                        },
                        {
                            id:  "cc",
                            key: "CC"
                        }
                        ],
                   groups: [
                        {
                            id:  "aa",
                            key: "AA"
                        },
                        {
                            id:  "bb",
                            key: "BB"
                        },
                        {
                            id:  "cc",
                            key: "CC"
                        }
                    ],
                    encrypted_data: "aaaaaaa"
                }
            }
        ]},
        {
            keyProvider: keySender
        }
    ).then((ret) => console.log(ret));
}

// main()
