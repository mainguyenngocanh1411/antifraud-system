const request = require('request');

const DB_HOST = 'http://115.165.166.126:1411' 
const PERMISSION_HOST = 'http://115.165.166.126:1411'  

function getTxListFromShipmentID(shipmentID, callback){
    let clientServerOptions = {
        url: DB_HOST + '/log/' + shipmentID,
        body: '',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }
    return request(clientServerOptions, function (error, response) {
        if (error) {
            console.error(error);
	        callback(error);
        }

        console.log(response.body);
	    callback(null, JSON.parse(response.body));
    });
}

async function postNewShipmentLog(data, callback) {
    let clientServerOptions = {
        url: DB_HOST + '/log/',
        body: '',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    }

    clientServerOptions.body = JSON.stringify({
        "USER_ID": data["USER_ID"],
        "GROUP_ID": data["GROUP_ID"],
        "TRANSACTION_ID": data["TRANSACTION_ID"]
    });

    return request(clientServerOptions, function (error, response) {
        if (error) {
            console.error(error);
            callback(error)
        }

        console.log(response.body);
        //return response.body;
        callback(null, JSON.parse(response.body)._id);
    });
}

async function alterShipmentLog(data) {
    let clientServerOptions = {
        url: DB_HOST + '/log/',
        body: '',
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    }

    clientServerOptions.body = JSON.stringify(data);

    return await request(clientServerOptions, function (error, response) {
        if (error) {
            console.error(error);
            return (error)
        }

        console.log(response.body);
        return response.body;
    });
}

function addNewPermissionName(name, callback) {
    let clientServerOptions = {
        url: PERMISSION_HOST + '/permissions/add/',
        body: '',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    } 

    clientServerOptions.body = JSON.stringify({
        name: name
    });

    return request(clientServerOptions, function(error, response) {
        if (error) {
            console.error(error);
	        callback(error);
        }

        console.log(response.body);
        callback(null, JSON.parse(response.body));
    })
}

function addUserToPermissionId(data, callback) {
    let clientServerOptions = {
        url: PERMISSION_HOST + '/permissions/add-user/',
        body: '',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    } 

    clientServerOptions.body = JSON.stringify({
        userId: data.USER_ID,
        permissionId: data.GROUP_ID
    });

    return request(clientServerOptions, function(error, response) {
        if (error) {
            console.error(error);
	        callback(error);
        }

        console.log(response.body);
        callback(null, JSON.parse(response.body));
    })
}

function getPermissionInfo(id, callback) {
    let clientServerOptions = {
        url: PERMISSION_HOST + '/permissions/' + id,
        body: '',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    } 

    return request(clientServerOptions, function(error, response) {
        if (error) {
            console.error(error);
	        callback(error);
        }

        console.log(response.body);
        callback(null, JSON.parse(response.body));
    })
}



function deleteUserFromPermisison(data, callback) {
    let clientServerOptions = {
        url: PERMISSION_HOST + '/permissions/:id',
        body: '',
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    } 

    clientServerOptions.body = JSON.stringify({
        "userId": data.USER_ID,
        "permissionId": data.GROUP_ID
    });

    return request(clientServerOptions, function(error, response) {
        if (error) {
            console.error(error);
	        callback(error);
        }

        console.log(response.body);
        callback(null, JSON.parse(response.body));
    })
}




module.exports = {
    getTxListFromShipmentID: getTxListFromShipmentID,
    postNewShipmentLog: postNewShipmentLog,
    alterShipmentLog: alterShipmentLog
}
