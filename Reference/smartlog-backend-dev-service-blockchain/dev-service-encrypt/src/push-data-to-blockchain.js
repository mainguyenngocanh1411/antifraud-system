const path = require('path')
const fs = require('fs')
const scphelper = require('./scpassword-helper')
const aes = require('./aes')
const enc = require('./data-encryptor')


function genSCPasswordAndEncryptData (input) {
    // let plainReceiverPublicKey   = fs.readFileSync(path.resolve('../key/pbkey-1.txt')).toString('utf8');

    let data = JSON.stringify(input,null,2)

    let saltForSCPassword   = aes.generateSalt();
    let scPassword          = aes.generateSCPassword();

    c = enc.encryptData(scPassword, data)
    console.log ("Encrypted data \n", c)

    saltedSCPassword = aes.saltSCPassword(scPassword, saltForSCPassword)

    return {
        encryptedData: c,
        saltedSCPassword: saltedSCPassword
    }
}

function encryptKeyForEachUser(saltedSCPassword, userId) {
    let plainSenderPrivateKey    = fs.readFileSync(path.resolve(__dirname + '/../key/2-pvkey.txt')).toString('utf8'); // HARDCODE

    let key = fs.readFileSync(__dirname + '/../key/' + userId + '-pbkey.txt', { encoding : 'utf8' }, function(err){
        if (err)
            return {
                "ERROR": "User " + userId + " doesn't exist. Reject!"
            }
    })
    return scphelper.encryptSCPassword(saltedSCPassword, plainSenderPrivateKey, key)
}

async function generateData(input, shipment_id) {
    ret = genSCPasswordAndEncryptData(input);

    return output = {
        "SHIPMENT_ID": shipment_id,
        "OWNER": {
            "ID": "OWNER",
            "KEY": encryptKeyForEachUser(ret.saltedSCPassword, "OWNER")
        },
        "ACTIVE": {
            "ID": "ACTIVE",
            "KEY": encryptKeyForEachUser(ret.saltedSCPassword, "ACTIVE")
        },
        "USERS": input["USER_ID"].map((id) => {
            return {
                "ID" : id,
                "KEY" : encryptKeyForEachUser(ret.saltedSCPassword, id)
            }
        }),
        "GROUPS": input["GROUP_ID"].map((id) => {
            return {
                "ID" : id,
                "KEY" : encryptKeyForEachUser(ret.saltedSCPassword, id)
            }
        }),
        "DATA": ret.encryptedData
    };
}

function test () {
    let input = JSON.parse(fs.readFileSync(path.resolve('../data/inp1.json'), {encoding: 'utf8'}))

    // let SHIPMENT_ID = 

    // input.SHIPMENT_ID = SHIPMENT_ID;
    delete input.USER_ID
    delete input.PUBKEY

    ret = encrypt(input);

    output = JSON.parse("{}");
    output.SHIPMENT_ID = input.SHIPMENT_ID;
    output.DATA = ret.encryptedData;
    output.KEYS = [];
    output.KEYS.push(ret.encryptedSaltedSCPassword)
    fs.writeFileSync(path.resolve('../enc/data1-enc.json'),JSON.stringify(output,null,2));
    // fs.writeFileSync(path.resolve('../enc/data1/key-data1.txt'), ret.encryptedSaltedSCPassword);
}


//  test()

module.exports.generateData = generateData;
