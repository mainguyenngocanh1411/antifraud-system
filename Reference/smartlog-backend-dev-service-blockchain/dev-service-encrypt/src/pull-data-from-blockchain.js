const path = require('path')
const fs = require('fs')
const scphelper = require('./scpassword-helper')
const aes = require('./aes')
const dec = require('./data-decryptor')

function decrypt(_input) { 
    let plainSenderPublicKey    = fs.readFileSync(path.resolve('../key/2-pbkey.txt')).toString('utf8');     // HARDCODE
    let plainReceiverPrivateKey   = fs.readFileSync(path.resolve('../key/1-pvkey.txt')).toString('utf8');

    saltedSCPassword = scphelper.decryptSCPassword(_input.key, plainReceiverPrivateKey, plainSenderPublicKey);
    scPassword = aes.unsaltSCPassword(saltedSCPassword);

    p = dec.decryptData(scPassword, _input.data)

    return p;
}

function decryptByUserId(_input, userId) {
    let plainSenderPublicKey    = fs.readFileSync(path.resolve(__dirname + '/../key/2-pbkey.txt')).toString('utf8');    // HARDCODE
    let plainReceiverPrivateKey   = fs.readFileSync(path.resolve(__dirname + '/../key/' + userId + '-pvkey.txt')).toString('utf8');

    saltedSCPassword = scphelper.decryptSCPassword(_input.key, plainReceiverPrivateKey, plainSenderPublicKey);
    scPassword = aes.unsaltSCPassword(saltedSCPassword);

    p = dec.decryptData(scPassword, _input.data)
    return p;
}

function decryptByUserPrivateKey(_input, plainReceiverPrivateKey) {
    let plainSenderPublicKey    = fs.readFileSync(path.resolve(__dirname + '/../key/2-pbkey.txt')).toString('utf8');    // HARDCODE

    saltedSCPassword = scphelper.decryptSCPassword(_input.key, plainReceiverPrivateKey, plainSenderPublicKey);
    scPassword = aes.unsaltSCPassword(saltedSCPassword);

    p = dec.decryptData(scPassword, _input.data)
    return p;
}

module.exports.decryptByUserPrivateKey = decryptByUserPrivateKey
module.exports.decryptByUserId = decryptByUserId
module.exports.decrypt = decrypt
