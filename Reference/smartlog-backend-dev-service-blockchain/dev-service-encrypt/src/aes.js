const CryptoJS = require("crypto-js");
const Crypto = require('crypto');

/**
 * RANDOM NUMBER GENERATOR
 */
function generateSalt() {
    return Crypto.randomBytes(6).toString('hex');
}

function generateSCPassword() {
    return Crypto.randomBytes(10).toString('hex');
}

/**
 * SALT THE SMART CONTRACT PASSWORD
 */

function saltSCPassword(_scPassword, _saltForSCPassword) {
    let saltBuffer = Buffer.from(_saltForSCPassword, 'hex');
    let scPasswordBuffer = Buffer.from(_scPassword, 'hex');

    var ret = Buffer.alloc(16,0);

    for (let i = 0; i < 6; i++) {
        saltBuffer.copy(ret, i*3, i, i+1);
        scPasswordBuffer.copy(ret, i*3 + 1, i*2, i*2+2);
    }

    console.log("[] Salted Password: ", ret.toString('hex'));
    
    return ret.toString('hex');
}

function unsaltSCPassword(_saltedSCPassword) {
    let saltedSCPasswordBuffer = Buffer.from(_saltedSCPassword,'hex')
    var ret = Buffer.alloc(10,0);

    for (let i = 0; i < 6; i++) {
        saltedSCPasswordBuffer.copy(ret, i*2, i*3+1, i*3+3)
    } 

    console.log("[] Unsalted Password: ", ret.toString('hex'))

    return ret.toString('hex');
}

/**
 * AES USING 
 */

function getKeyAndIVofAES (_password) {

    let keyBitLength = 256;
    let ivBitLength = 128;
    let iterations = 234;
    
    // var saltForKeyAndIV = CryptoJS.lib.WordArray.create(_saltForSCPassword)
    let saltForKeyAndIV = '02bac013d8a19e';     // HARD-CODE HERE - LIKE AN INITIAL FOR IV

    let iv128Bits  = CryptoJS.PBKDF2(_password, saltForKeyAndIV, { keySize: ivBitLength / 32, iterations: iterations });
    let key256Bits = CryptoJS.PBKDF2(_password, saltForKeyAndIV, { keySize: keyBitLength / 32, iterations: iterations });

    return {
        iv: iv128Bits,
        key: key256Bits
    };
};


module.exports.generateSalt       = generateSalt
module.exports.generateSCPassword = generateSCPassword
module.exports.saltSCPassword     = saltSCPassword
module.exports.unsaltSCPassword   = unsaltSCPassword
module.exports.getKeyAndIVofAES   = getKeyAndIVofAES