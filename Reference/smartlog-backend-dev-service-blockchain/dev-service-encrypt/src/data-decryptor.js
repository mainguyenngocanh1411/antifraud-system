const CryptoJS = require("crypto-js");
const aes = require('./aes')

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function decryptData(scPassword, message) {

    let skey = aes.getKeyAndIVofAES(scPassword);
    let key = CryptoJS.enc.Base64.parse(skey.key.toString(CryptoJS.enc.Base64));
    let iv = CryptoJS.enc.Base64.parse(skey.iv.toString(CryptoJS.enc.Base64));

    let params = {
        ciphertext: CryptoJS.enc.Base64.parse(message),
        salt: ""
    };

    let plainText = CryptoJS.AES.decrypt(params, key, { iv: iv });

    return plainText.toString(CryptoJS.enc.Utf8).replaceAll("\n", "");
}

module.exports.decryptData = decryptData
