const crypto = require("crypto");
const eccrypto = require("eccrypto");

/**
 * Generating a totally new key pair
 * 
 * @param 
 * @returns {JSON} {Buffer, Buffer} A pair of keys in Buffer type
 */
function generatePairKey () {
    var privateKey = crypto.randomBytes(32);
    var publicKey  = eccrypto.getPublic(privateKey);

    console.log("Generated Private Key: ", privateKey.toString('hex'))
    console.log("Generated Public Key:  ", publicKey.toString('hex'))

    return {
        privateKey: privateKey,
        publicKey: publicKey
    }
}

/**
 * Recovering PublicKey from Private Key
 * _privateKey must be a string and valid private key as secp256k1 form
 * 
 * Function raises error if input is a bad private key 
 * 
 * @param {String} _privateKey  
 * @returns {JSON} {Buffer, Buffer} A pair of keys in Buffer type
 * 
 * @example <caption>Successful recovering</caption>
 * genaratePublicKeyFromPrivateKey("efe571c0073b8e09083098552c90db54c71bd7cb572b201869e211f7f02068cb")
 * 
 * // Print
 * //     Input Private Key:      efe571c0073b8e09083098552c90db54c71bd7cb572b201869e211f7f02068cb
 * //     Generated Public Key:   046030b1207097a0ce2bda36a322124534e107de158f9786e66f699e3a89cc63bde27b0f9e02f39dfdd9be138852ee8c9656f382a567282e3fb9a6bbe252320175
 * 
 * // Return
 * //     privateKey:   <Buffer ef e5 71 c0 07 3b 8e 09 08 30 98 55 2c 90 db 54 c7 1b d7 cb 57 2b 20 18 69 e2 11 f7 f0 20 68 cb>
 * //     publicKey:    <Buffer 04 60 30 b1 20 70 97 a0 ce 2b da 36 a3 22 12 45 34 e1 07 de 15 8f 97 86 e6 6f 69 9e 3a 89 cc 63 bd e2 7b 0f 9e 02 f3 9d fd d9 be 13 88 52 ee 8c 96 56 ... >
 *
 * @example <caption>Fail recovering</caption>
 * genaratePublicKeyFromPrivateKey("1")
 * 
 * // Print and Return
 * //    { ERROR: 'Bad private key' }
 */
function genaratePublicKeyFromPrivateKey (_privateKey) {
    let privateKey = Buffer.from(_privateKey, 'hex')
    let publicKey;
    
    try {
        publicKey = eccrypto.getPublic(privateKey);
    } catch (error) { 
        console.error(error)
        return {
            ERROR: "Bad private key"
        }
    }

    console.log("Input Private Key:     ", privateKey.toString('hex'))
    console.log("Generated Public Key:  ", publicKey.toString('hex'))

    return {
        privateKey: privateKey,
        publicKey:  publicKey
    }
}

module.exports = {generatePairKey, genaratePublicKeyFromPrivateKey}