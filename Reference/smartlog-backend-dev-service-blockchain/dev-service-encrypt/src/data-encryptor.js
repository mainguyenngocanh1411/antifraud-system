const CryptoJS = require("crypto-js");
const aes = require('./aes')

function encryptData(scPassword, message) {
    plainText = message.toString('hex');

    let skey = aes.getKeyAndIVofAES(scPassword);
    let data = CryptoJS.AES.encrypt(plainText, skey.key, { iv: skey.iv });
    let ciphertext = data.ciphertext.toString(CryptoJS.enc.Base64);

    return ciphertext;
}


module.exports.encryptData = encryptData