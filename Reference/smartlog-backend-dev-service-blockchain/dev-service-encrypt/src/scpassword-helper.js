const bitcore = require('bitcore-lib');
const ECIES = require('bitcore-ecies');

function encryptSCPassword (_scPassword, plainSenderPrivateKey, plainReceiverPublicKey) {

    if (plainSenderPrivateKey === '')
        plainSenderPrivateKey    = '22945f3632ec0b5ec3d46a9a2a21b5015aa6190f6df3ddf53b6bd132beb2b337'       // HARDCODE
    let senderPrivateKey         = new bitcore.PrivateKey.fromString(plainSenderPrivateKey); 

    // let plainReceiverPublicKey   = fs.readFileSync(path.resolve('../key/pbkey-1.txt')).toString('utf8');
    let receiverPublicKey        = new bitcore.PublicKey.fromString(plainReceiverPublicKey, 'hex');

    var sender = ECIES()
    .privateKey(senderPrivateKey)
    .publicKey(receiverPublicKey);
  
    var encryptedSCPassword = sender.encrypt(_scPassword);

    console.log("ENCRYPT SMART CONTRACT PASSWORD MODULE")
    console.log("[] Salted Smart Contract Password:    ", _scPassword);
    console.log("[] Encrypted Smart Contract Password: ", encryptedSCPassword.toString('base64'));
    console.log("[] Public Key of Receiver:            ", receiverPublicKey.toString());
    console.log("")


    return encryptedSCPassword.toString('base64');
}

function decryptSCPassword (_encSCPassword, plainReceiverPrivateKey, plainSenderPublicKey) {
    // plainReceiverPrivateKey = fs.readFileSync(path.resolve('../key/pvkey-1.txt')).toString('utf8')

    let receiverPrivateKey   = new bitcore.PrivateKey.fromString(plainReceiverPrivateKey, 'hex');

    if (plainSenderPublicKey === '')            // HARDCODE
        plainSenderPublicKey = '044c052827eaf74d547f73d55128d49b83a30fc9b97f8890bce060eeaaab428b622ae18d6b9630d201cbddcef1a595f4049208d40d3b224fa03274c686bef07fcf'
    let senderPublicKey      = new bitcore.PublicKey.fromString(plainSenderPublicKey); 

    var receiver = ECIES()
        .privateKey(receiverPrivateKey)
        .publicKey(senderPublicKey);

    var scPassword = receiver
        .decrypt(Buffer.from(_encSCPassword, 'base64'))
        .toString();

    console.log("DECRYPT SMART CONTRACT PASSWORD MODULE")
    console.log("[] Salted Smart Contract Password: ", scPassword);

    return scPassword;
}

// encryptSCPassword('deadbeefdeadbeefdead')
// decryptSCPassword('A0wFKCfq901Uf3PVUSjUm4OjD8m5f4iQvOBg7qqrQotiJ9E7948oj8eTy081fT+oXMNWeYums8ZtU4+ktF66+gIWPld/601RfPJ15OPK/qUXJK/VtOsV6L4sP8rD07hvRZErsV94DWwEIBUC4aZ2yCQ=')


module.exports.encryptSCPassword = encryptSCPassword
module.exports.decryptSCPassword = decryptSCPassword
