# Decryption Library

## Term definitions
- `plain data`: original data, human-readable data sent from user
- `cipher data`: encrypted data of `plain data`, which is publicly written inside blockchain's data 

- `scPassword` (a.k.a. `scKey`, smart contract password...): password used to encrypt a piece of data which is pushed to blockchain via smart contract
- `saltedPassword = scPassword + salt`: password added some salt-bytes
- `decryptedSaltedSCPassword = decrypt(saltedPassword, userPublicKey)`: decrypted password by a user's public key, this password will be stored in blockchain along with encrypted data

## Features
1. Generating key pair
2. Decrypting `scSaltedPassword` from `decryptedSaltedSCPassword`
3. Unsalt `scPassword` from `scSaltedPassword` 
4. Decrypting plain data [of contract] from `scPassword` and cipher data
5. Merge code for step (2-5)

## Usage

This library exports some module for external calls only. There is **NO** interactive UI or console-based for this lib.
All examples below are written in [./src](./src) folder 

**NOTES:** All keys (include private key and public key) must be in `hex` form **without** `prefix '0x'` 

### Generating key pair
```js
const KeyPairGenerator = require('./private-and-public-keys-generator')

// Generate a totally new a pair of keys
// The new key pair is in Buffer type
keyPair = KeyPairGenerator.generatePairKey()

// Generate/Recover public key from a private key
keyPairRecover = KeyPairGenerator.genaratePublicKeyFromPrivateKey("6c24c761296d91cc308ea46509d2c92d5a862aa75a6d018819cbedb008fda0fd")
```

Sample result 
```
Generated Private Key:  eaeb55de2e996bb38513585c6ea3f68c59a433848bce749dba19d2771ab1dd9e
Generated Public Key:   043604127e7b85565c2532754a3ff2cb882f58bcf1d812f67748ce94494cb60299a66d07d198c269e76d77c3cd6ffa3e29cbb47283cfa286f582c59ed11677e640
Input Private Key:      efe571c0073b8e09083098552c90db54ca1bd7cb572b201869e211f7f02068cb
Generated Public Key:   0413a90680c26845c36ac63501ffbb1401cc8c30a551607cab11a505a6ae8851a031343a3dbd4d28ca6d1a4f2789b16196ae834032d5c433ecf0b0e7c7db14f45c
```

### Decrypt scPassword from decryptedSaltedSCPassword

```js
const SCPassword = require('./scpassword-helper')

encryptedSCPassword = "A0wFKCfq901Uf3PVUSjUm4OjD8m5f4iQvOBg7qqrQotiJ9E7948oj8eTy081fT+oXMNWeYums8ZtU4+ktF66+gIWPld/601RfPJ15OPK/qUXJK/VtOsV6L4sP8rD07hvRZErsV94DWwEIBUC4aZ2yCQ="
receiverPrivateKey  = "3f9ee2e5556727e2f0f1f2d2a9989b0a0612f6367782045e9bc2e09dcd1f0c60"

SCPassword.decryptSCPassword(encryptedSCPassword,receiverPrivateKey,'')

encryptedSCPassword = "A0wFKCfq901Uf3PVUSjUm4OjD8m5f4iQvOBg7qqrQoti510lBF/ml/ZC0IpOGajWYnyAJF68GW48mTf+ZIKtJm08D1JciqEwmRrEGQEvVqLZZptHdZ5JgQGhs1g+3IhU2ZgGIR5CEvJeumrjSPWSUsAT3Z+AbKRzPTwu88pVBIG9"
SCPassword.decryptSCPassword(encryptedSCPassword,receiverPrivateKey,'')
```

Result
```
DECRYPT SMART CONTRACT PASSWORD MODULE
[] Salted Smart Contract Password:  deadbeefdeadbeefdead

DECRYPT SMART CONTRACT PASSWORD MODULE
[] Salted Smart Contract Password:  c4a97044384764f5ec9861dea3e89c02
```

### Unsalt `scPassword` from `scSaltedPassword` 
```js
const AES = require('./aes')

saltedSCPassword = "c4a97044384764f5ec9861dea3e89c02"
unsaltPassword = AES.unsaltSCPassword(saltedSCPassword)
```

Result
```
[] Unsalted Password:  a9703847f5ec61dee89c
```

### Decrypting plain data of contract from `scPassword` and cipher data
```js
const DataDecryptor = require('./data-decryptor')

scPassword = "a9703847f5ec61dee89c"
cipherData = "nwhp+RjIkOln+yRYir54UeuSB7SsuJmgLwum2SwHmbr+WGeojgSmyNN5iJ+p8Nc92vxMbKUSlZxuIyCTDQPobkLA11kywavKzRW8ryjjTm8Af+TAiFbptQfKfXNavHEUlUyVL9YEdLkNkTM3ixtCQHqWATJ+sejZESPnslFCqARzKPl6wa80754d6qFqx2URixhSFpjWCavntmT/S2Zul5THfKnVByiCrUcx5cMUiMFnBkJqc05k6+QI63g65GopKB0MTXDuMpi6VDcyYAwjShrHfEwkUrWz+0rrs+EZo+C4VFto3k/N+OU5VR/9GC8HauIlypwOHkUfQMF47XWxKtzFKCX+yFQRN7xCiPujfILULYimYCthIXhA1lR5y/NQoT/ciVmYMc0583OdaeLighw5Q2K8H++o+xBOdaMtuUNSjP+ttxxz5kaHm0YdWkJfWl8Zm+WgI3yEKbEaNneB1TYeaTfl8V27Kt3mNWBaxvSIQPlAbqNj0gSuDjfyFKCpt6kRfg8J9hruu8YUcmC9dHER3eOFqT30UWD8i2xnoR5aBNLpBZcwjmJKjJk1glmn+qgrd2o/TTOCnsgN69foCnHWtd/cyTsPxcmU+FQDBaEg9NpL2sJ00BCshowretv9eE2OMuGq4FPF2kYwLwe6CES93cFhS76RmB42+urfpqmq6GhI+/AM1+wN/t5DqFAwBsTNYz/mXlH2qo65Rh74b+OTas2/FAOBe6VbVjr1O6zarvWtD6VMNfiWSQpNe369nNB/6fV+xBmxuQS2gvIYF+1ORQy1vUcdmJ7DQ9SJDZrIsQJM4ZP6IkyYlbFGX4ZMh/bQ1NCQ09t9OXWZpFCocv/+kf57XJbUtMbwMWnFbyqd+I69kRfgeY+O/SUfteX4BV6jq7UcHpqfFfDFVL135lvB7AgHPkjAxte/et3dzU5OZakERM45MtjDntkZwslnW6L480fkL8qCGSlA+2Lamq64274+kE20X28nY7nUwu9CaZgcHi7fcvHNtCh+GDZAkW21cBV98JBQF5+xZuPKSTthU8V17FaSj2UJbjKxmf7jxAypgq3MzZaR7rctbbtYHv9Cr/NNcpMJuWnf8OmgF+RxIkknCy82Uh0E8REwq5Kn+/ZOSg/bK9S4GlfYd/9VS40MLAyf6lHqVqO3lLL1UlrVDoCZN2RXqP8AV4Ty70Ifwh3bgmCzmOv3hJzVwc+KXpOhs7ie7gppAJfuug8m6pcwDXpYdS2F+j9/Wahjk3RUsqLmy+wremfAuKTBnwOpL/UUdYvCfKxvNpOpGUyNi7JRazkzG3U1Gz3pL0oFUWOLYXod9v2ysQJGjYqXqkeQ3JU5SomkJcq34EkkOmRCWBSjRmJcNBNeIa4mqm8SUDy9FT7eNsn9i2Y1sjSTWTyyD4fgOq6d8wyrjCYcADScny8PRjCxwRFZBQEB9j9yie02ScoYxoDl8ObEeEjnEB5OkWZvVESaUaN1TUBdvci4cPEtURV7VOS/Q8deuow92BUwW3hKGnNfg5Lb9YXfSH56YNSY9XzmfqVJ+WJkReRK9eTvGnVR83fxMliPlM92oCNPmMvd7NEcZi7Y0SWPUaVtqvDZsAKNKW8XX4IB3QptF+n2CiPdvnxgSVUkE50K3fstK0quvhD0E0yp0HOjD+DH5+cbDItpCuOpY9/S7mC3x/LQeBbIAgVd89/J0aTZps7sA8ej74krO/pg4JWyjNEmufBdIb1sHiIIn14NpF+mKZMtJ/joLtbvw3yFT4BKm1L5Fw/M1gKVosfZqG8kUzoxzXnni/rjOYieVx5bSR8zhE+QWmduiys6cmZ8+np403I5hY2TdJyZPxvquVhSiWJfGE0vMuchnycyiXA1eNJUcyAvZ9LwW25RpiLUtJUSJLPV00ukgeoqrJGUeqOQdgLSdEe0brULtzpJoUIyVegDzYwWc9ch5pEijAzFUxqqpyIX4XURrMjEyzAjm/TvsXB4eaYoYQ4S9Y8qKgyGcwlsxWhaHxsqWo7MixUBpnv3rMxcyivHdb8v5yABYVt1euuND1xG2QnXfi7UxlkHmfHr29U2N/DKs8Qu/50JWlUe1C0LAh4xEHbStZ+2IPXSDrr0TMUK+VdJk7jkWeGef9r4mhNmn/2copzD/Q84YTkgLhd1meF5CWvK++BSMMWMQk4Vpvl+G54Wsbu6N81V5hfG+yg4O3R0Hru2Z66AdV7VpMUXnIiMs0IDiAiVZ+xm2UVhUkfYfxDntr5FGqXP3I48Rj2xVofZInEMDm1hOI/ysiQZW+XpwhyHLIXIwbc5Rf4k"

plain = DataDecryptor.decryptData(scPassword, cipherData)
console.log(plain)
```

Result (snapshot)
```
{  "SHIPMENT_ID": 1,  "DATA": {    "WHSEID": "wmwhse1",    "SCHEDULEDATE": "2018-07-19T17:00:00.000Z",    "DETAILS": [      {        "WHSEID": "wmwhse1",        "TYPE": "INBOUND",        "PRIORITY": "", "OPENQTY": 0,        "OPENNW": 0,        "OPENGW": 0,        "OPENCBM": 0,        "GATE": "",        "DOORTYPE": "",        "DOOR": "",        "STATUS": "",        "BEGINTIME": "2018-07-19T10:00:00.000Z",    "ENDTIME": "2018-07-19T11:00:00.000Z",
      .....
"DRIVERNAME": "",        "DRIVERPHONE": "",        "RECEIPTKEY": "",        "EXTERNRECEIPTKEY": "",        "SUPPLIERCODE": "",        "ORDERKEY": "1800000003",        "EXTERNORDERKEY": "POCUSTOMER",        "CUSTOMERCODE": "CUS01"      }    ]  }}
```

### Merge the three above steps
Library currently supports these methods to decrypt any data
- [x] Decrypt by `USER_ID`. `USER_ID` is located as plaintext in `./key/<USER_ID>-pvkey.txt`. (E.g. Key of user [`CARR001`](./key/CARR001-pvkey.txt))
- [x] Decrypt by `OWNER`. [`OWNER`'s key](./key/OWNER-pvkey.txt) is held by system by default
- [x] Decrypt by key manually inputed by user

```js 
const Decryptor = require('./pull-data-from-blockchain')

input = {
    key: "A0wFKCfq901Uf3PVUSjUm4OjD8m5f4iQvOBg7qqrQoti510lBF/ml/ZC0IpOGajWYnyAJF68GW48mTf+ZIKtJm08D1JciqEwmRrEGQEvVqLZZptHdZ5JgQGhs1g+3IhU2ZgGIR5CEvJeumrjSPWSUsAT3Z+AbKRzPTwu88pVBIG9",
    data: "nwhp+RjIkOln+yRYir54UeuSB7SsuJmgLwum2SwHmbr+WGeojgSmyNN5iJ+p8Nc92vxMbKUSlZxuIyCTDQPobkLA11kywavKzRW8ryjjTm8Af+TAiFbptQfKfXNavHEUlUyVL9YEdLkNkTM3ixtCQHqWATJ+sejZESPnslFCqARzKPl6wa80754d6qFqx2URixhSFpjWCavntmT/S2Zul5THfKnVByiCrUcx5cMUiMFnBkJqc05k6+QI63g65GopKB0MTXDuMpi6VDcyYAwjShrHfEwkUrWz+0rrs+EZo+C4VFto3k/N+OU5VR/9GC8HauIlypwOHkUfQMF47XWxKtzFKCX+yFQRN7xCiPujfILULYimYCthIXhA1lR5y/NQoT/ciVmYMc0583OdaeLighw5Q2K8H++o+xBOdaMtuUNSjP+ttxxz5kaHm0YdWkJfWl8Zm+WgI3yEKbEaNneB1TYeaTfl8V27Kt3mNWBaxvSIQPlAbqNj0gSuDjfyFKCpt6kRfg8J9hruu8YUcmC9dHER3eOFqT30UWD8i2xnoR5aBNLpBZcwjmJKjJk1glmn+qgrd2o/TTOCnsgN69foCnHWtd/cyTsPxcmU+FQDBaEg9NpL2sJ00BCshowretv9eE2OMuGq4FPF2kYwLwe6CES93cFhS76RmB42+urfpqmq6GhI+/AM1+wN/t5DqFAwBsTNYz/mXlH2qo65Rh74b+OTas2/FAOBe6VbVjr1O6zarvWtD6VMNfiWSQpNe369nNB/6fV+xBmxuQS2gvIYF+1ORQy1vUcdmJ7DQ9SJDZrIsQJM4ZP6IkyYlbFGX4ZMh/bQ1NCQ09t9OXWZpFCocv/+kf57XJbUtMbwMWnFbyqd+I69kRfgeY+O/SUfteX4BV6jq7UcHpqfFfDFVL135lvB7AgHPkjAxte/et3dzU5OZakERM45MtjDntkZwslnW6L480fkL8qCGSlA+2Lamq64274+kE20X28nY7nUwu9CaZgcHi7fcvHNtCh+GDZAkW21cBV98JBQF5+xZuPKSTthU8V17FaSj2UJbjKxmf7jxAypgq3MzZaR7rctbbtYHv9Cr/NNcpMJuWnf8OmgF+RxIkknCy82Uh0E8REwq5Kn+/ZOSg/bK9S4GlfYd/9VS40MLAyf6lHqVqO3lLL1UlrVDoCZN2RXqP8AV4Ty70Ifwh3bgmCzmOv3hJzVwc+KXpOhs7ie7gppAJfuug8m6pcwDXpYdS2F+j9/Wahjk3RUsqLmy+wremfAuKTBnwOpL/UUdYvCfKxvNpOpGUyNi7JRazkzG3U1Gz3pL0oFUWOLYXod9v2ysQJGjYqXqkeQ3JU5SomkJcq34EkkOmRCWBSjRmJcNBNeIa4mqm8SUDy9FT7eNsn9i2Y1sjSTWTyyD4fgOq6d8wyrjCYcADScny8PRjCxwRFZBQEB9j9yie02ScoYxoDl8ObEeEjnEB5OkWZvVESaUaN1TUBdvci4cPEtURV7VOS/Q8deuow92BUwW3hKGnNfg5Lb9YXfSH56YNSY9XzmfqVJ+WJkReRK9eTvGnVR83fxMliPlM92oCNPmMvd7NEcZi7Y0SWPUaVtqvDZsAKNKW8XX4IB3QptF+n2CiPdvnxgSVUkE50K3fstK0quvhD0E0yp0HOjD+DH5+cbDItpCuOpY9/S7mC3x/LQeBbIAgVd89/J0aTZps7sA8ej74krO/pg4JWyjNEmufBdIb1sHiIIn14NpF+mKZMtJ/joLtbvw3yFT4BKm1L5Fw/M1gKVosfZqG8kUzoxzXnni/rjOYieVx5bSR8zhE+QWmduiys6cmZ8+np403I5hY2TdJyZPxvquVhSiWJfGE0vMuchnycyiXA1eNJUcyAvZ9LwW25RpiLUtJUSJLPV00ukgeoqrJGUeqOQdgLSdEe0brULtzpJoUIyVegDzYwWc9ch5pEijAzFUxqqpyIX4XURrMjEyzAjm/TvsXB4eaYoYQ4S9Y8qKgyGcwlsxWhaHxsqWo7MixUBpnv3rMxcyivHdb8v5yABYVt1euuND1xG2QnXfi7UxlkHmfHr29U2N/DKs8Qu/50JWlUe1C0LAh4xEHbStZ+2IPXSDrr0TMUK+VdJk7jkWeGef9r4mhNmn/2copzD/Q84YTkgLhd1meF5CWvK++BSMMWMQk4Vpvl+G54Wsbu6N81V5hfG+yg4O3R0Hru2Z66AdV7VpMUXnIiMs0IDiAiVZ+xm2UVhUkfYfxDntr5FGqXP3I48Rj2xVofZInEMDm1hOI/ysiQZW+XpwhyHLIXIwbc5Rf4k"
}

// This case only works if `SCKEY_FOR_OWNER` exists in `input`
// plain = Decryptor.decrypt(input)
// console.log(plain)

plain = Decryptor.decryptByUserId(input, "1")
console.log(plain)

plann = Decryptor.decryptByUserPrivateKey(input, "3f9ee2e5556727e2f0f1f2d2a9989b0a0612f6367782045e9bc2e09dcd1f0c60")
console.log(plain)
```

Result (snapshot of `decryptByUserId`)
```
DECRYPT SMART CONTRACT PASSWORD MODULE
[] Salted Smart Contract Password:  c4a97044384764f5ec9861dea3e89c02
[] Unsalted Password:  a9703847f5ec61dee89c

{  "SHIPMENT_ID": 1,  "DATA": {    "WHSEID": "wmwhse1",    "SCHEDULEDATE": "2018-07-19T17:00:00.000Z",    "DETAILS": [      {        "WHSEID": "wmwhse1",        "TYPE": "INBOUND",        "PRIORITY": "", "OPENQTY": 0,        "OPENNW": 0,        "OPENGW": 0,        "OPENCBM": 0,        "GATE": "",        "DOORTYPE": "",        "DOOR": "",        "STATUS": "",        "BEGINTIME": "2018-07-19T10:00:00.000Z",    "ENDTIME": "2018-07-19T11:00:00.000Z",
      .....
"DRIVERNAME": "",        "DRIVERPHONE": "",        "RECEIPTKEY": "",        "EXTERNRECEIPTKEY": "",        "SUPPLIERCODE": "",        "ORDERKEY": "1800000003",        "EXTERNORDERKEY": "POCUSTOMER",        "CUSTOMERCODE": "CUS01"      }    ]  }}
```