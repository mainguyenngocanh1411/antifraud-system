import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Student;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

class AddStudents {


    private JFileChooser fileChooser;


    AddStudents() {

        fileChooser = new JFileChooser();
        fileChooser.setBounds(50, 50, 500, 400);

    }

    public void execute() {
        int returnValue = fileChooser.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {

            File selectedFile = fileChooser.getSelectedFile();
            Workbook wb;
            try {
                wb = WorkbookFactory.create(selectedFile);
                Sheet sheet = wb.getSheet("students");

                sheet.forEach(row -> {

                    if (row.getRowNum() != 0) {
                        Student student = new Student(row.getCell(0).toString().replace(".0", ""), row.getCell(1).toString(),row.getCell(2).toString(),"");
                        createRequest(student);

                    }

                });


            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private String createRequest(Student student) {
        System.out.println("Adding student " + student.getStdId());
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://localhost:1411/std");
        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_STRING;
        try {
            JSON_STRING = objectMapper.writeValueAsString(student);
            StringEntity requestEntity = new StringEntity(
                    JSON_STRING,
                    ContentType.APPLICATION_JSON);
            CloseableHttpResponse response;
            try {
                post.setEntity(requestEntity);
                response = httpclient.execute(post);
                System.out.println(response.getStatusLine());
                //response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return null;

    }


}