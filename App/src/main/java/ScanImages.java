import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.*;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import javax.swing.*;
import java.io.IOException;

import static org.opencv.imgcodecs.Imgcodecs.IMREAD_COLOR;

public class ScanImages {

    ScanImages() {

    }

    /**
     * This method get information from image
     * and create transacion push data to blockchain
     * then update database
     * @param scanResult
     */
    public void execute(ListReponse scanResult) {

        //Get public key of student
        String publicKey = getPublicKey(scanResult.getStudentId());

        //Get public key of examiner
        String EPublicKey = getExaminersPublicKey(scanResult.getStudentId());


        String scKey = AESEncryptor.randomScKey();
        System.out.println("Generate sckey " + scKey);
        //IPFS
        Mat source = Imgcodecs.imread("/home/na/Desktop/LVTN/App/src/main/java/models/sheet-5.jpg", IMREAD_COLOR);

        try {
            String encrypt = ImageEncryptor.encrypt(source, scKey);

            IPFSService ipfsService = new IPFSService();
            String imageHash = ipfsService.addSring(encrypt);
            imageHash = imageHash.split("-")[0];

            //Encrypte message with SC key
            EncryptMessage encryptMessage = new EncryptMessage(scanResult.getAnswer(), scKey);
            String encryptedMessage = AESEncryptor.encrypt(encryptMessage);

            //Encrypt SC key with public key of students and examiners
            String encryptSecrectKey1 = encryptData(scKey,publicKey);
            String encryptSecrectKey2 = encryptData(scKey, EPublicKey);
            Transaction transaction = new Transaction("123",
                    "LY2",
                    scanResult.getStudentId(),
                    encryptSecrectKey1,
                    encryptSecrectKey2,
                    "abc",
                    encryptedMessage,
                    imageHash); //TODO use secret key to lock answer

            createRequest(transaction);
        } catch (IOException e) {
            e.printStackTrace();
        }






    }
    private String encryptData(String secretKey, String publicKey) {
        Encrypt encryptData = new Encrypt(secretKey, publicKey);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://localhost:1411/cryptor/Encrypt");
        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_STRING;
        String encryptKey = "";
        try {
            JSON_STRING = objectMapper.writeValueAsString(encryptData);
            StringEntity requestEntity = new StringEntity(
                    JSON_STRING,
                    ContentType.APPLICATION_JSON);
            CloseableHttpResponse response;
            try {
                post.setEntity(requestEntity);
                response = httpclient.execute(post);
                encryptKey = EntityUtils.toString(response.getEntity());
                System.out.println("encrypt " + encryptKey);
                //response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }



    return encryptKey;
    }

    /**
     * Get public key of students
     * @param studentId
     * @return
     */
    private String getPublicKey(String studentId){
        String publicKey ="";
        CloseableHttpClient httpclient = HttpClients.createDefault();
       HttpGet get =  new HttpGet("http://localhost:1411/std/"+studentId);
        CloseableHttpResponse response;
        try {
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine());

           Student student = new ObjectMapper().readValue(EntityUtils.toString(response.getEntity()), Student.class);
            publicKey = student.getPublicKey();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    /**
     * Get public key of examiners
     * @param studentId
     * @return
     */
    private String getExaminersPublicKey(String studentId){
        String publicKey ="";

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet get =  new HttpGet("http://localhost:1411/std/key/"+studentId);
        CloseableHttpResponse response;
        try {
            response = httpclient.execute(get);
            System.out.println(response.getStatusLine());

            Key key = new ObjectMapper().readValue(EntityUtils.toString(response.getEntity()), Key.class);
            publicKey = key.getPublicKey();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return publicKey;
    }

    private String createRequest(Transaction transaction) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://127.0.0.1:1411/tx/push");
        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_STRING;
        try {
            JSON_STRING = objectMapper.writeValueAsString(transaction);
            StringEntity requestEntity = new StringEntity(
                    JSON_STRING,
                    ContentType.APPLICATION_JSON);
            CloseableHttpResponse response;
            try {
                post.setEntity(requestEntity);
                response = httpclient.execute(post);
                System.out.println(response);
                //response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return null;

    }

    void getImage(String hash, String privateKey, String stdId){
        System.out.println("Hash " + hash);
        System.out.println("privateKey " + privateKey);
        System.out.println("stdId " + stdId);
        //get sckey
        Decrypt decryptData = new Decrypt(stdId, privateKey);
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://localhost:1411/cryptor/getSckey");
        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_STRING;
        String scKey;
        try {
            JSON_STRING = objectMapper.writeValueAsString(decryptData);
            StringEntity requestEntity = new StringEntity(
                    JSON_STRING,
                    ContentType.APPLICATION_JSON);
            CloseableHttpResponse response;
            try {
                post.setEntity(requestEntity);
                response = httpclient.execute(post);
                scKey = EntityUtils.toString(response.getEntity());
                //get encrypted image
                IPFSService ipfsService = new IPFSService();
                String encryptedImage = ipfsService.getString(hash);

                System.out.println("Image " + encryptedImage);
                System.out.println("Sckey: " + scKey);

                //Decrypt
                Mat decrypt = null;
                try {
                    decrypt = ImageEncryptor.decrypt(encryptedImage, scKey, 1087, 1211);
                    Util.write2File(decrypt, "recovered.png");
                    JFrame frame = new JFrame();
                    ImageIcon icon = new ImageIcon("/home/na/Desktop/LVTN/App/target/recovered.png");
                    JLabel label = new JLabel(icon);
                    frame.add(label);
                    frame.setDefaultCloseOperation
                            (JFrame.EXIT_ON_CLOSE);
                    frame.pack();
                    frame.setVisible(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }



    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        ImageIcon icon = new ImageIcon("/home/na/Desktop/LVTN/App/target/recovered.png");
        JLabel label = new JLabel(icon);
        frame.add(label);
        frame.setDefaultCloseOperation
                (JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
