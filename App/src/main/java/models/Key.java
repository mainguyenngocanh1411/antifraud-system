package models;

public class Key {
    private String _id;
    private String publicKey;
    private String privateKey;

    public Key(){

    }
    public Key(String id, String publicKey, String privateKey) {
        _id = id;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}
