package models;

public class EncryptMessage {
    private String message;
    private String scKey;

    public EncryptMessage(String message, String scKey){
        this.message = message;
        this.scKey = scKey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getScKey() {
        return scKey;
    }

    public void setScKey(String scKey) {
        this.scKey = scKey;
    }
}
