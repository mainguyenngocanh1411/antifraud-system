package models;

public class Decrypt {
    private String stdId;
    private String privateKey;


    public Decrypt(String stdId,
                   String privateKey) {
        this.stdId = stdId;
        this.privateKey = privateKey;

    }


    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }


}
