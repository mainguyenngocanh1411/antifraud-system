package models;

public class StudentIdOnly {
    private String id;

    public StudentIdOnly(String id) {
        this.id = id;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}