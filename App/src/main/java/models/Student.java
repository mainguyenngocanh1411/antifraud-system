package models;

public class Student {
    private String stdId;
    private String name;
    private String dateOfBirth;
    private String publicKey;
    public Student(){

    }
    public Student(String stdId, String name, String dateOfBirth, String publicKey) {
        this.stdId = stdId;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.publicKey = publicKey;
    }


    public String getStdId() {
        return stdId;
    }

    public void setStdId(String stdId) {
        this.stdId = stdId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
