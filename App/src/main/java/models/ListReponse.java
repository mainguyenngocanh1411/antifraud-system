package models;

public class ListReponse {
    private String studentId;
    //TODO Encrypt
    private String answer;

    public ListReponse(String studentId, String answer) {
        this.studentId = studentId;
        this.answer = answer;

    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
