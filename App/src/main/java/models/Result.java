package models;

public class Result {
    private String exam_code;
    private String subject_code;
    private String student_id;
    private String DATA;

    public Result(String exam_code,
                  String subject_code,
                  String student_id,
                  String DATA) {
        this.exam_code = exam_code;
        this.subject_code = subject_code;
        this.student_id = student_id;
        this.DATA = DATA;
    }


    public String getExam_code() {
        return exam_code;
    }

    public void setExam_code(String exam_code) {
        this.exam_code = exam_code;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getDATA() {
        return DATA;
    }

    public void setDATA(String DATA) {
        this.DATA = DATA;
    }
}
