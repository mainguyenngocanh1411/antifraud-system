package models;

public class Encrypt {
    private String message;
    private String publicKey;


    public Encrypt(String message,
                   String publicKey) {
        this.message = message;
        this.publicKey = publicKey;

    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }


}
