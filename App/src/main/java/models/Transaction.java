package models;

public class Transaction {
    private String exam_code;
    private String subject_code;
    private String student_id;
    private String secretKey1;
    private String secretKey2;
    private String secretKey3;
    private String DATA;
    private String imageHash;

    public Transaction(String exam_code,
                       String subject_code,
                       String student_id,
                       String secretKey1,
                       String secretKey2,
                       String secretKey3,
                       String DATA,
                       String imageHash) {
        this.exam_code = exam_code;
        this.subject_code = subject_code;
        this.student_id = student_id;
        this.secretKey1 = secretKey1;
        this.secretKey2 = secretKey2;
        this.secretKey3 = secretKey3;
        this.DATA = DATA;
        this.imageHash = imageHash;
    }


    public String getExam_code() {
        return exam_code;
    }

    public void setExam_code(String exam_code) {
        this.exam_code = exam_code;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getSecretKey1() {
        return secretKey1;
    }

    public void setSecretKey1(String secretKey1) {
        this.secretKey1 = secretKey1;
    }

    public String getSecretKey2() {
        return secretKey2;
    }

    public void setSecretKey2(String secretKey2) {
        this.secretKey2 = secretKey2;
    }

    public String getSecretKey3() {
        return secretKey3;
    }

    public void setSecretKey3(String secretKey3) {
        this.secretKey3 = secretKey3;
    }

    public String getDATA() {
        return DATA;
    }

    public void setDATA(String DATA) {
        this.DATA = DATA;
    }

    public String getImageHash() {
        return imageHash;
    }

    public void setImageHash(String imageHash) {
        this.imageHash = imageHash;
    }
}
