import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

public class ImageEncryptor {

  static {
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
  }

  public static final String ENCRYPT_URL = "http://localhost:1411/cryptor/encryptAES";
  public static final String DECRYPT_URL = "http://localhost:1411/cryptor/decryptAES";
  static Mat source;

  public static void main(String[] args) {
    source = Imgcodecs.imread(Util.getSource("sheet-5.jpg"));

    try {
      String encrypt = encrypt(source, "12345");
//      System.out.println("Encrypted: " + encrypt);
      Mat decrypt = decrypt(encrypt, "12345", source.height(), source.width());
      Util.write2File(decrypt, "recovered.png");
      System.out.println("size:" + source.height() + "x" + source.width());
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static String encrypt(Mat imageToEncrypt, String scKey)
      throws IOException {

    byte[] return_buff = new byte[(int) (imageToEncrypt.total() *
        imageToEncrypt.channels())];
    imageToEncrypt.get(0, 0, return_buff);

    byte[] compress = compress(return_buff);

    String message = Arrays.toString(compress);

    CloseableHttpClient client = HttpClients.createDefault();
    HttpPost httpPost = new HttpPost(ENCRYPT_URL);

    String json = "{"
        + "\"message\":\"" + message
        + "\",\"scKey\":\"" + scKey +"\"}";

//    System.out.println(json);
    StringEntity entity = new StringEntity(json);
    httpPost.setEntity(entity);
    httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");

    CloseableHttpResponse response = client.execute(httpPost);
    String result = EntityUtils.toString(response.getEntity());
    client.close();

    return result;

  }

  public static Mat decrypt(String encryptedMessage, String scKey, int height, int width)
      throws IOException {
    CloseableHttpClient client = HttpClients.createDefault();
    HttpPost httpPost = new HttpPost(DECRYPT_URL);

    String json = "{"
        + "\"encryptedMessage\":" + encryptedMessage + ","
        + "\"scKey\":" + scKey + ""
        + "}";
    System.out.println("JSON"+ json);
    StringEntity entity = new StringEntity(json);
    httpPost.setEntity(entity);
    httpPost.setHeader("Accept", "application/json");
    httpPost.setHeader("Content-type", "application/json");

    CloseableHttpResponse response = client.execute(httpPost);
    System.out.println(response.getEntity());
    String result = EntityUtils.toString(response.getEntity());

//    System.out.println("Decrypted: " + result);
    client.close();

    byte[] raw_data = fromString(result);

    StringUtils.getBytesUtf8(result);

    try {
      byte[] decompress = decompress(raw_data);
      System.out.println("Decompress length: " + decompress.length);
      Mat mat = new Mat(height,width, CvType.CV_8UC3);
      mat.put(0, 0, decompress);
      System.out.println(mat.size());
      return mat;
    } catch (DataFormatException e) {
      e.printStackTrace();
    }

    return null;
  }

  public static byte[] compress(byte[] data) throws IOException {
    Deflater deflater = new Deflater();
    deflater.setInput(data);
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
    deflater.finish();
    byte[] buffer = new byte[1024];
    while (!deflater.finished()) {
      int count = deflater.deflate(buffer); // returns the generated code... index
      outputStream.write(buffer, 0, count);
    }
    outputStream.close();
    byte[] output = outputStream.toByteArray();
    System.out.println("Original: " + data.length);
    System.out.println("Compressed: " + output.length);
    return output;
  }

  public static byte[] decompress(byte[] data) throws IOException, DataFormatException {
    Inflater inflater = new Inflater();
    inflater.setInput(data);
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
    byte[] buffer = new byte[1024];
    while (!inflater.finished()) {
      int count = inflater.inflate(buffer);
      outputStream.write(buffer, 0, count);
    }
    outputStream.close();
    byte[] output = outputStream.toByteArray();
    System.out.println("Original: " + data.length);
    System.out.println("Decompressed: " + output.length);
    return output;
  }

  private static byte[] fromString(String string) {
    System.out.println("Test");

    String[] strings = string.replace("\"", "").replace("[", "").replace("]", "").split(", ");
    byte result[] = new byte[strings.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = (byte) Integer.parseInt(strings[i]);
    }
    return result;
  }
//
//  @Test
//  public void test() {
//    String s = "\"[123, 210]\"";
//    System.out.println(Arrays.toString(fromString(s)));
//  }

}
