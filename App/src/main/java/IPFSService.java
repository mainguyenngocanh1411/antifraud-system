import java.io.IOException;

import io.ipfs.api.IPFS;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multihash.Multihash;

public class IPFSService {
    private IPFS ipfs;
    IPFSService(){
        this.ipfs =  new IPFS("/ip4/127.0.0.1/tcp/5001");
        try {
            ipfs.refs.local();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String addSring(String image) {
        byte[] imageBytes = image.getBytes();
        NamedStreamable.ByteArrayWrapper file = new NamedStreamable.ByteArrayWrapper(imageBytes);
        MerkleNode addResult = null;
        try {
            addResult = ipfs.add(file).get(0);
            System.out.println(addResult);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (addResult!=null) ? addResult.toString() : null;

    }

    public String getString(String hash) {
//        System.out.println("Hash is " + hash);
        Multihash filePointer = Multihash.fromBase58(hash);
        try {

            byte[] fileContents = ipfs.cat(filePointer);
            return new String(fileContents);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



}
