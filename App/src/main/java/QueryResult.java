import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.StudentIdOnly;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import javax.swing.*;
import java.io.IOException;

class QueryResult {


    private JTextField input;
    private JFrame frame;
    private JLabel examCode;
    private JLabel studentId;
    private JLabel subjectCode;
    private JLabel result;
    private JButton ok;


    QueryResult() {

        frame = new JFrame();
        input = new JTextField();
        examCode = new JLabel();
        studentId = new JLabel();
        subjectCode = new JLabel();
        result = new JLabel();
        ok = new JButton("Query");
        input.setBounds(200, 100, 200, 50);//x axis, y axis, width, height
        ok.setBounds(200, 200, 200, 50);//x axis, y axis, width, height
        studentId.setBounds(210, 300, 200, 50);//x axis, y axis, width, height
        examCode.setBounds(210, 350, 200, 50);//x axis, y axis, width, height
        subjectCode.setBounds(210, 400, 200, 50);//x axis, y axis, width, height
        result.setBounds(210, 450, 200, 50);//x axis, y axis, width, height

        frame.add(input);
        frame.add(studentId);
        frame.add(subjectCode);
        frame.add(result);
        frame.add(examCode);
        frame.add(ok);
        frame.setSize(600, 600);
        frame.setLayout(null);
        frame.setVisible(true);

    }

    public void execute() {

        ok.addActionListener(actionEvent -> {
            String id = input.getText();
            String request = createRequest(id);
            System.out.println(request);

        });

    }

    private String createRequest(String id) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost post = new HttpPost("http://127.0.0.1:1412/tx/pull");
        ObjectMapper objectMapper = new ObjectMapper();
        String JSON_STRING;
        StudentIdOnly student = new StudentIdOnly(id);
        try {
            JSON_STRING = objectMapper.writeValueAsString(student);
            System.out.println(JSON_STRING);
            StringEntity requestEntity = new StringEntity(
                    JSON_STRING,
                    ContentType.APPLICATION_JSON);
            CloseableHttpResponse response;
            try {
                post.setEntity(requestEntity);
                response = httpclient.execute(post);
                JsonNode jsonNode = objectMapper.readTree(response.getEntity().getContent());

                studentId.setText("student id: " + jsonNode.get("studentId").toString());               examCode.setText("exam code: " + jsonNode.get("examCode").toString());
                subjectCode.setText("subject code: " + jsonNode.get("subjectCode").toString());
                examCode.setText("exam code: " + jsonNode.get("examCode").toString());
                result.setText("Answer: " + jsonNode.get("encrypted_answers").toString());
                //response.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return null;

    }


}