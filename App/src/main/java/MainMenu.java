import models.ListReponse;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import javax.swing.*;
import java.awt.*;

import static org.opencv.imgcodecs.Imgcodecs.IMREAD_COLOR;

public class MainMenu {

    static {

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

    }


    public static void main(String[] args) {


        JFrame frame = new JFrame();
        JButton addStudents = new JButton("Add students ");
        addStudents.setBounds(200, 200, 200, 50);//x axis, y axis, width, height
        JButton scanImages = new JButton("Scan images");
        scanImages.setBounds(200, 300, 200, 50);
        JButton getImage = new JButton("Get image");
        getImage.setBounds(200, 400, 200, 50);
        JLabel title = new JLabel("<html> Application of scaning <br /> and handling tests </html>");
        title.setFont(new Font("Serif", Font.PLAIN, 30));
        title.setBounds(140,100,500,70);
        frame.add(title);
        frame.add(getImage);
        frame.add(addStudents);
        frame.add(scanImages);
        frame.setSize(600, 600);
        frame.setLayout(null);
        frame.setVisible(true);

        addStudents.addActionListener(actionEvent -> {
            AddStudents addStudent = new AddStudents();
            addStudent.execute();
        });

        getImage.addActionListener(actionEvent -> {

            JFrame getImageFrame= new JFrame();
            getImageFrame.setTitle("Get image from IPFS");
            getImageFrame.setSize(600, 600);
            getImageFrame.setLayout(null);
            getImageFrame.setVisible(true);
            JLabel subTitle = new JLabel("Get image from IPFS");
            subTitle.setFont(new Font("Serif", Font.PLAIN, 30));
            subTitle.setBounds(140,90,500,70);
            getImageFrame.add(subTitle);

            JLabel studentInput = new JLabel("Student Id: ");
            studentInput.setBounds(200, 160, 200, 50);
            JLabel hashInput = new JLabel("Image hash: ");
            studentInput.setBounds(200, 160, 200, 50);
            JLabel keyInput = new JLabel("Private key: ");
            studentInput.setBounds(200, 160, 200, 50);
            hashInput.setBounds(200,260,200,50);
            keyInput.setBounds(200,360,200,50);
            //Create input
            JTextField stdId = new JTextField();
            JTextField hash = new JTextField();
            JTextField privateKey = new JTextField();
            JButton ok = new JButton("Get");
            stdId.setBounds(200, 200, 200, 50);

            hash.setBounds(200, 300, 200, 50);
            privateKey.setBounds(200, 400, 200, 50);
            ok.setBounds(200, 500, 200, 50);
            getImageFrame.add(keyInput);
            getImageFrame.add(hashInput);
            getImageFrame.add(studentInput);
            getImageFrame.add(stdId);
            getImageFrame.add(hash);
            getImageFrame.add(privateKey);
            getImageFrame.add(ok);
            ok.addActionListener(actionEvent1 -> {
                System.out.println("Getting image");
                ScanImages scanImagesService = new ScanImages();
                scanImagesService.getImage(hash.getText(),
                        privateKey.getText(),
                        stdId.getText());
            });
        });

        scanImages.addActionListener(actionEvent -> {
            ScanImages scanImage = new ScanImages();
            System.out.println("...started");

            Mat source = Imgcodecs.imread("/home/na/Desktop/LVTN/App/src/main/java/models/sheet-5.jpg", IMREAD_COLOR);
            System.out.println(source);


            // Image process
            Scanner scanner = new Scanner(source);
            scanner.setLogging(false);
            ListReponse scan = scanner.scan();


            scanImage.execute(scan);
            Util.sout("...finished");
        });

    }

}
